
let s:bufnr = -1

let s:events = [
			\ {
				\ 'instr': 1,
				\ 'start': 0,
				\ 'dur': 1,
				\ 'ps': [],
				\ },
			\ {
				\ 'instr': 0,
				\ 'start': 1,
				\ 'dur': 1,
				\ 'ps': [],
				\ },
			\ {
				\ 'instr': 0,
				\ 'start': 5,
				\ 'dur': 0,
				\ 'ps': [],
				\ },
				\ ]


func! Make_buf ()
	let bufnr = bufadd ('tsco')
	call bufload ('tsco')
	return bufnr
endfunc

" Draws the tracker.
func! Draw ()
	" Note to the voices in my head. You could
	normal! ggdG
	let i = 0
	let top_line = '        '
	while i < 6
		let top_line .= i . '        '
		let i += 1
	endwhile 
	0put =top_line
	echom line ('.')
	let i = 0
	while i < 10
		put =i
		let i += 1
	endwhile
	let i = 0
	while i < len (s:events)
		exe 1 + s:events[i].start + 1
		exe 'normal! a  ' 
		normal! Rhi
		let i += 1
	endwhile
endfunc

func! Set_bindings ()
	nnoremap <buffer> q :echom "hi"<cr>
endfunc

" Makes the buf it doesn't exist, jumps to it.
func! Run ()
	if s:bufnr == -1
		let s:bufnr = Make_buf ()
		exe 'sb' s:bufnr
		call Set_bindings ()
		setlocal bufhidden=hide buftype=nofile noswapfile
	else
		let winid = bufwinid ('tsco')
		if winid == -1
			exe 'sb' s:bufnr
		endif
	endif
endfunc

call Run ()
call Draw ()
