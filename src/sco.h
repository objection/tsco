#pragma once

#include "types.h"

struct orc {
	char *path;
	int instr;
};

$make_arr_with_member (struct orc, orcs, int i);

struct symbolic_note {
	enum fc_note_name name;

	// Or maybe yet another union with "cents". Or maybe none of this,
	// just a double that's turned into note name plus cents

	$flt offset;
};

// These are not "low level" types. They're not, eg HEX or SYMBOLIC. I
// just need a reasonable way to calculate where the cursor should be.
// It's all fixed: note, instr, amp, then arbitrary p-fields. So, it's
// not entirely fixed. It's the arbitrary p-fields that are pissing me
// off. I'd love to define everything with nice, big x-macro. But I
// can't quite.

enum p_field_type {
	PFT_NOTE,
	PFT_INSTR,
	PFT_AMP,
	PFT_P_FIELD,
	PFT_N,
};

extern int p_field_widths[PFT_N];

struct p_field {
	/* union { */
	/* 	$flt fltval; */
	/* 	int hexval; */
	/* 	enum fc_note_name note_name; */
	/* }; */
	$flt val;
	int id;
};
$make_arr (struct p_field, ps);

struct event {
	// Up until now (2021-05-10), events have been sorted just
	// by their start value. This has caused problems -- really
	// the problem is me failing to sort floats.
	//
	// Luckily I can sidestep this. I recall in some trackers
	// can do stuff like this:
	//
	// 		C-4 01 FF d01
	//
	// I don't think d is the actual effect name. It's a delay, or
	// offset. This would delay this note until the start of the next
	// note. I found uses for this.
	//
	// Anyway, this "row", corrosponds to the row, obviously. And the
	// start is when the note is actually played. You're only allowed
	// one event per row, but you could delay a previous note so it
	// starts at the same time as another. Of course, in that case,
	// you'd probably want to put it on another channel.
	int row;
	int instr;
	$flt start;
	$flt dur;
	$flt cps;
	$flt amp;
	struct ps ps;
};

$make_arr_with_member (struct event, channel, int most_ps);
$make_arr (struct event, events);
$make_arr (struct channel, channels);

typedef struct sco {
	char *path;
	struct channels channels;
	struct channels deleted; // An undo stack.
	$flt row_len; // In csound seconds.
	$flt tempo;
	struct v curs; // y: row, x: channel.
	i8 p_field_offset;
	// Cursor index into the shell.
	// Like, 0 for instr, 1 for note, 2 for amp, and the rest
	// are your arbitrary p-fields.
	i8 curs_p_field;

	// This is in cells. It's the max start instr (x) and max start
	// (or dur, not decided)

	struct v dims;
} sco;

enum letter_reges {
	LR_A, LR_B, LR_C, LR_D, LR_E, LR_F, LR_G, LR_H, LR_I, LR_J, LR_K, LR_L, LR_M, LR_N,
	LR_O, LR_P, LR_Q, LR_R, LR_S, LR_T, LR_U, LR_V, LR_W, LR_X, LR_Y, LR_Z,

	// Watch out for this lowercase n. I always call my number-of-enum
	// enum WHATEVER_N. But LR_N is a register.

	LR_n,
};
struct reges {
	struct events letters[LR_n];

	// NOTE: I'm not going to bother with 1..9. At least for now.
	//
	// The way I see it, it's made redundant by infinite undo. If
	// anyone ever reads this, complain to me on gitlab and I'll stick
	// it in.
	//
	// I do think I can afford to be a little opinionate about Vim. A
	// lot of stuff in Vim is never used any more -- at least by me;
	// I'm guessing -- because they came up with better ways to do
	// things later on.


	// zero is the last yanked text.
	struct events zero;

	// The Vim help says the unnamed register "is like the unnamed
	// register is pointing to the last used register", so lets you an
	// actual pointer.

	struct events *unnamed;
};

$make_arr_with_member (
		struct sco, scos,
		struct {
			int i;
			struct reges reges;
		});
extern struct scos scos;
extern struct orcs orcs;

$ncmp_dec_by_member_using_func (sco, path, char *, ncmp_strs);

struct sco sco_make_default_sco (void);
int sco_cmp_event_by_row (const void *_a, const void *_b);
struct event sco_make_event (int instr, int channel, int row, $flt start);
struct event *get_prev_event (int start_from);
struct event *get_next_event (int start_from);
struct event *get_event_at_row (struct channel *channel,
		int start_time);
struct event *get_last_event (void);
void shift_events (struct channel *channel, int from_row, int n_rows);
int read_sco_or_tsco (struct sco *sco, char *path, bool is_tsco);
char *write_sco_to_buf (size_t *n_res);
int write_tsco (struct sco *sco, char *path);
void free_event (struct event *event);
void free_channel (struct channel *channel);
