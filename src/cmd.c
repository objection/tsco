#define _GNU_SOURCE
#include "cmd.h"
#include "sco.h"
#include "ui.h"
#include "misc.h"

// This just gets a buf from scratch_get and sticks t.s in there. It
// only needs to be done for the 0. Remember to call $scratch_unget.
#define $put_t_in_scratch(t) \
	({ \
		char *_buf = $scratch_get (PATH_MAX); \
		sprintf (_buf, "%.*s", (int) (t->e - t->s), t->s); \
	 	_buf; \
	})

enum cmd_t_type {
	CTT_ADDR,
	CTT_SEMICOLON,
	// This is used for subcmds, too.
	CTT_CMD_SC,
	CTT_VALUE,
	CTT_ERROR,
	CTT_END,
};

// I'm lumping subcommands in here. So, "add" isn't a a "toplevel"
// command. It's one you use with "orc" and "sco". Maybe I'm being
// lazy. Maybe it's better for this to be defined in data. But.
#define CMDS \
	x (CTC_QUIT, "quit") \
	x (CTC_QA, "qa") \
	x (CTC_WRITE, "write") \
	x (CTC_WA, "wa") \
	x (CTC_WRITE_QUIT, "wq") \
	x (CTC_EDIT, "edit") \
	x (CTC_SCO, "sco") \
	x (CTC_PLAY, "play") \
	x (CTC_ORC, "orc") \
	x (CTC_DEL, "del") \
	x (CTC_SET, "set") \
	x (CTC_CD, "cd") \
	x (CTC_PWD, "pwd") \
	x (CTC_N, 0) \

#define x(a, b) a,
enum cmd_t_cmd {CMDS};
#undef x
#define x(a, b) b,
static char *cmd_strs[] = {CMDS};
#undef x

#define ORC_SC_SUBCMDS \
	x (ORC_SC_ADD, "add") \
	x (ORC_SC_DELL, "del") \
	x (ORC_SC_EDIT, "edit") \
	x (ORC_SC_INSTR, "instr") \
	x (ORC_SC_N, 0)
#define x(a, b) a,
enum orc_sc {ORC_SC_SUBCMDS};
#undef x
#define x(a, b) b,
static char *orc_sc_strs[] = {ORC_SC_SUBCMDS};
#undef x

#define SCO_SUBCMDS \
	x (SCO_SC_ADD, "add") \
	x (SCO_SC_DEL, "del") \
	x (SCO_SC_EDIT, "edit") \
	/* I feel like it'd be more explict/proper/nice/pretty/neat */ \
	/* if I had you do "sco set tempo 200" instead of */ \
	/* "sco tempo 200". But it's pretty long-winded. */ \
	x (SCO_SC_TEMPO, "tempo") \
	x (SCO_SC_EXPORT, "export") \
	x (SCO_SC_N, 0)

#define x(a, b) a,
enum sco_sc {SCO_SUBCMDS};
#undef x
#define x(a, b) b,
static char *sco_sc_strs[] = {SCO_SUBCMDS};
#undef x

#define SET_SUBCMDS \
	x (SET_OCTAVE, "octave") \
	x (SET_NOTATION, "notation") \
	x (SET_N, 0)

#define x(a, b) a,
enum set_sc {SET_SUBCMDS};
#undef x
#define x(a, b) b,
static char *set_sc_strs[] = {SET_SUBCMDS};
#undef x

#define SET_NOTATION_SUBCMDS \
	\
	/* Let's not bother with there for now. They're there to help you */ \
	/* write scores, but I can't imagine you'll find much use for them */ \
	/* here. You want "symbol", and you'll defnitely want freq. */ \
	/* Probably midi, but I won't bother with that for now, either. */ \
	\
	/* x (SET_NOTATION_PCH, "pch") \ */ \
	/* x (SET_NOTATION_OCT, "oct") \ */ \
	x (SET_NOTATION_SYMBOLIC, "symbolic") \
	x (SET_NOTATION_CPS, "cps") \
	/* x (SET_NOTATION_MIDI, "midi") \ */ \
	x (SET_NOTATION_N, 0)

#define x(a, b) a,
enum set_notation_sc {SET_NOTATION_SUBCMDS};
#undef x
#define x(a, b) b,
static char *set_notation_sc_strs[] = {SET_NOTATION_SUBCMDS};
#undef x

struct cmd_t {
	char *s, *e;
	enum cmd_t_type type;
	union {
		enum cmd_t_cmd cmd;
		enum orc_sc orc_sc;
		enum sco_sc sco_sc;
		enum set_sc set_sc;
		enum set_notation_sc set_notation_sc;
	};
};

void free_line (struct line *line) { free (line->d); }

enum cmd_t_get_what {
	CTGW_CMD_SUBCMD,
	CTGW_VALUE,
};

// Goes through through the token char by char, checking if each
// cmd_t_str still matches. If it still matches at the end, it's a
// match.
static int match_cmd_sc (struct cmd_t *t, int n_cmds, char **cmd_strs, char *end) {

	int r = 0;

	while (t->e < end && !isspace (*t->e))
		t->e++;
	int n_matching = n_cmds;

	// Represents the matching cmds.
	bool matching[n_cmds];

	// Start off by pretending everything matches. Which, I suppose is
	// true.
	$each (match, matching, n_cmds)
		*match = 1;

	// Go through char by char.
	int i;
	for (i = 0; i < t->e - t->s; i++) {
		$each (cmd_str, cmd_strs, n_cmds) {
			if (matching[cmd_str - cmd_strs]) {

				// If the cmd str stops matching, set its matching
				// elem to 0
				if ((*cmd_str)[i] != t->s[i]) {
					matching[cmd_str - cmd_strs] = 0;
					n_matching -= 1;
				}
			}
		}

		// If you've got just one match, you've won.
		if (i == t->e - t->s && n_matching == 1)
			break;
	}
	if (0 == n_matching || t->s + i != t->e) {
		$print_msg_get_out ("No command matching \"%.*s\"", t->e - t->s, t->s);
		t->type = CTT_ERROR;
	}
	else if (n_matching > 1) {
		$print_msg_get_out ("Too many matches for \"%.*s\"", t->e - t->s, t->s);
		t->type = CTT_ERROR;
	} else { // n_matching == 1
		t->type = CTT_CMD_SC;

		// I could avoid a loop by using a stack or something. Or a
		// pointer.
		$each (match, matching, n_cmds) {
			if (*match) {
				t->cmd = match - matching;
				break;
			}
		}
	}
out:
	return r;
}

// You tell it what kind of cmds/subcmds to match against. The reason
// for this is I, like Vim, am letting you write, eg, w instead of
// write. I'm looking at what you've witten and giving you the closest
// match. Not a fuzzy match: an initial-string match, you know. You
// type "w", it matches write because that's (right now) the only cmd
// that matches. Later if there's a "wombat" command, you'd have to
// type "wr".
//
// Anyway, this means I can't just stick all the cmds in an array and
// match against them. Because there's subcmds. You type "orc", and
// you can follow it up with "add", "dell",
// or whatever.
static struct cmd_t cmd_t_get (char *p, char *end, int n_cmd_strs, char **cmd_strs,
		enum cmd_t_get_what get_what) {
	struct cmd_t r = {.s = p};
	skipnblank (&r.s, end - p);

	if (r.s >= end) {
		r.e = r.s;
		r.type = CTT_END;
		return r;
	}
	r.e = r.s + 1;

	if (get_what == CTGW_VALUE) {
		if (*r.s == '"') {
			while (r.e < end && !(*r.e == '"' && *(r.e - 1) != '\\'))
				r.e++;
			if (*r.e != '"') {
				print_msg ("Unterminated string");
				r.type = CTT_ERROR;
				return r;
			}
		} else {
			while (r.e < end && !isspace (*r.e))
				r.e++;
		}
		r.type = CTT_VALUE;
		return r;
	}

	switch (*r.s) {
		case ';':
			r.type = CTT_SEMICOLON;
			break;
		default:
			match_cmd_sc (&r, n_cmd_strs, cmd_strs, end);
			break;
	}
	return r;
}


// do_export means write a normal sco file. do_export = 0 means write
// a tsco file, which is basically the same thing but with stuff to
// show where the channels are.
static int cmd_write_export (struct cmd_t *t, char *end, bool do_export) {
	FILE *f = 0;
	int r = 0;
	char *sco_buf = 0;
	assert (scos.n);
	struct sco *sco = &scos.d[scos.i];
	*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
	char *path = 0;
	path = $scratch_get (PATH_MAX);
	if (t->type == CTT_ERROR)
		$tsco_go ();
	// If you've just done :write
	else if (t->type == CTT_END) {
		if (!sco->path)
			$print_msg_get_out ("Buffer %d doesn't have a name", scos.i);
		// Don't need this copy, but it's simpler, since I can just
		// scratch unget.
		snprintf (path, PATH_MAX, "%s", scos.d[scos.i].path);
	} else
		snprintf (path, PATH_MAX, "%.*s", (int) (t->e - t->s), t->s);

	if (!sco->path)
		sco->path = strdup (path);

	if (do_export) {

		size_t n_res;

		// I could just write straight to the file, but in play I
		// write to a buffer. I could use a function that takes a
		// function pointer, but I've tried that and it's a pain.
		sco_buf = write_sco_to_buf (&n_res);
		if (!sco_buf)
			$print_msg_get_out ("Couldn't write the score");

		f = fopen (path, "w");
		if (!f)
			$print_msg_get_out ("Couldn't open %s", path);

		if (n_res != fwrite (sco_buf, 1, n_res, f))
			$print_msg_get_out ("Error writing to %s: %s",
					path, strerror (errno));
	} else
		write_tsco (sco, path);

out:
	if (path)
		$scratch_unget ();
	if (sco_buf) free (sco_buf);
	if (f) fclose (f);
	return r;
}

int start_play (char *sco_path, char *orc_path) {
	int r = 0;
	char *sco_buf = 0, *orc_buf = 0;

	// If you've not specified a score or the score you've specified
	// is the current buffer, use what's in the buffer. Don't read it
	// in from the file. That'd be confusing, I think. Maybe this is a
	// bad subtlety.
	//
	// Also, this function can be called with f5, so use current sco
	// if null pointer.
	if (!sco_path || !*sco_path) {
#if 0 // Use this for when I have more than one buffer.
		struct buffer *matching_sco =
			lfind (&(struct buffer) {.path = sco_path}, scos->d, &scos->n,
					sizeof *scos->d, cmp_buffer_by_path);
		if (matching_sco) {
			size_t n_sco_buf;
			sco_buf = write_sco_to_buf (&n_sco_buf);
		}
#endif
	} else
		sco_buf = $slurp_od (scos.d[scos.i].path);

	// This function can be called with f5, so use current orc if null
	// pointer -- as well as the !*orc_path that you might get from
	// cmd_play.
	if (!orc_path || !*orc_path) {
		if (!orcs.n)
			$print_msg_get_out ("You haven't added any orcs");

		// Don't hang on to the orc text. The user will change it.
		orc_buf = $slurp_od (orcs.d[orcs.i].path);
		if (!orc_buf)
			$print_msg_get_out ("Couldn't read \"%s\": %s",
					orcs.d[orcs.i].path, strerror (errno));
	}

	size_t n_sco_buf;
	sco_buf = write_sco_to_buf (&n_sco_buf);

	int ret = csoundCompileOrc (cs, orc_buf);
	if (ret)
		$print_msg_get_out ("Couldn't compile orchestra %s", orc_path);
	if ((ret = csoundReadScore (cs, sco_buf)))
		$print_msg_get_out ("Couldn't compile score %s", sco_path);
	csoundSetOption (cs, "-odac");
	csoundCreateMessageBuffer (cs, 0);
	if (csoundStart (cs))
		print_msg ("Couldn't start csound");

out:
	$free_if (sco_buf);
	$free_if (orc_buf);
	return r;
}


//
// :play works like this:
//
// If you just do :play, it will play the current buffer (sco file)
// and the current orc.
//
// If you do :play with an orc and/or a sco, it'll use whatever you
// give it. But if you do, eg, two orcs, that's an error. I think
// that's fine. If you want to use two orcs, you can include them
// in the orc file.
//
static int cmd_play (struct cmd_t *t, char *end) {
	int r = 0;
	*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);

	// Going to copy the str here, whether it's necessary or not. It
	// isn't strictly necessary if it comes from, eg,
	// orcs->d[orcs->i];. But it makes the function neater. And
	// cleanliness if next to godliness.
	char sco_path[PATH_MAX];
	char orc_path[PATH_MAX];

	// Use a zero in the path to mean not set yet.
	*sco_path = 0;
	*orc_path = 0;

	// Error msg should be already given
	if (t->type == CTT_ERROR)
		$tsco_go ();

	// Loop getting all the .scos and .orcs. Really, I'm just looking
	// for one of each.
	while (t->type != CTT_END) {
		snprintf (sco_path, PATH_MAX, "%.*s", (int) (t->e - t->s), t->s);
		if (fnmatch ("*\\.sco", sco_path, 0)) {
			if (*sco_path) {
				print_msg ("You've already got a sco");
				$tsco_go ();
			}
		} else if (fnmatch ("*\\.orc", orc_path, 0)) {
			if (*orc_path) {
				print_msg ("You've already got a sco");
				$tsco_go ();
			}
		} else {
			print_msg ("%.*s doesn't look like .sco or .orc",
					(int) (t->e - t->s), t->s);
			$tsco_go ();
		}
		*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
	};

	start_play (sco_path, orc_path);
out:
	return r;
}

static int cmd_orc (struct cmd_t *t, char *end) {
	int r = 0;
	*t = cmd_t_get (t->e, end, ORC_SC_N, orc_sc_strs, CTGW_CMD_SUBCMD);
	struct orc *orc = orcs.n? &orcs.d[orcs.i]: 0;
	if (t->type != CTT_CMD_SC)
		return 1;
	switch (t->orc_sc) {
		case ORC_SC_ADD:
			*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);

			char *orc_path = 0;
			asprintf (&orc_path, "%.*s",
					(int) (t->s - t->e), t->s);
			if (-1 == access (orc_path, F_OK)) {
				print_msg ("\
So called \"orchestra file\" \"%s\", doesn't exist", orc_path);
				free (orc_path);
				$tsco_go ();
			}
			arr_push (&orcs, (struct orc) {.path = orc_path});
			break;
		case ORC_SC_EDIT:
			*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
			if (t->type == CTT_END) {
				if (!orc)
					$print_msg_get_out ("No orcs loaded");
				else
					$print_msg_get_out ("%s", orc->path);

				systemf ("$EDITOR %s", orc->path);
			}
			break;
		case ORC_SC_INSTR:
			*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
			// Can't use $get_int_or_get_out here,
			// unless I make the macro more sophisticated
			if (t->type == CTT_END)
				$print_msg_get_out ("%d", orc->instr);
			$get_int_or_get_out (&orc->instr, t->s, "instr", 1, INT_MAX);
			orc->instr--;
			// FIXME: this should check if you've entered a value
			// < the number of instr. I don't know how to. csound
			// will have a function to get the number of instrs.
			// But it shouldn't error. You might be about to add
			// the orc. Csound will give you an error if it's a
			// genuine mistake.
			break;
		default:
			print_msg ("%s isn't valid", cmd_strs[t->cmd]);
			break;
	}
out:
	return r;
}

static int cmd_sco_edit (struct cmd_t *t, char *end) {
	int r = 0;
	*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
	char *path = $put_t_in_scratch (t);
	struct sco *sco = arr_find (&scos, (void *) &(struct sco) {.path = path},
			ncmp_scos_by_path_using_ncmp_strs);
	if (sco)
		scos.i = sco - scos.d;
	else {
		struct sco sco = {0};
		if (read_sco_or_tsco (&sco, path, 1))
			$tsco_go ();
		arr_push (&scos, sco);
		scos.i = scos.n - 1;
	}
out:
	$scratch_unget ();
	return r;
}

static int cmd_sco (struct cmd_t *t, char *end) {
	int r = 0;
	struct sco *sco = &scos.d[scos.i];
	*t = cmd_t_get (t->e, end, SCO_SC_N, sco_sc_strs, CTGW_CMD_SUBCMD);
	if (t->type != CTT_CMD_SC)
		return 1;
	switch (t->sco_sc) {
		case SCO_SC_EDIT:
			cmd_sco_edit (t, end);
			break;
		case SCO_SC_TEMPO:
			*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
			if (t->type == CTT_END)

				// TODO: get this to not print stuff after decimal
				// point if there's nothing. The printf manpage
				// appears to say this is what happens by default, but
				// contradicts itself.
				//
				// And I quote!
				//
				// "# The value should be converted to an "alternate
				// form" ... For a, A, e, E, f, F, g, and G
				// conversions, the result will always contain a
				// decimal point, even if  no digits  follow  it
				// (normally, a decimal point appears in the results
				// of those conversions only if a digit follows)."
				//
				// But, later, it says that 6 digits will always
				// follow, unless you specify otherwise.
				$print_msg_get_out ("%lf", sco->tempo);
			$get_flt_or_get_out (&sco->tempo, t->s, "tempo", -1, -1);
			break;
		case SCO_SC_EXPORT:
			cmd_write_export (t, end, 1);
			break;
		default: break;
	}
out:
	return r;
}

// "set" is for options to do with this editor. It's not for things
// you might write to the score file, eg "tempo". So, "octave" is
// set here, because it's an editor thing: it's nothing that's
// written to the score file.
//
// Later there might be buffer-local variables. Right now, they're
// all global.
static int cmd_set (struct cmd_t *t, char *end) {
	*t = cmd_t_get (t->e, end, SET_N, set_sc_strs, CTGW_CMD_SUBCMD);
	struct ed *ed = &ui.editor.ed;
	int r = 0;
	if (t->type != CTT_CMD_SC)
		return 1;
	switch (t->set_sc) {
		case SET_OCTAVE:
			*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
			if (t->type == CTT_END)
				$print_msg_get_out ("%d", ed->octave);
			$get_int_or_get_out (&ed->octave, t->s, "octave", $min_octave, $max_octave);
			break;
		case SET_NOTATION:
			*t = cmd_t_get (t->e, end, SET_NOTATION_N, set_notation_sc_strs,
					CTGW_CMD_SUBCMD);
			if (t->type == CTT_END)
				$print_msg_get_out ("%s", set_notation_sc_strs[ed->notation]);

			// TODO: let you have separate notation for events,
			// channels and tune. This note could have this notation.
			// If it doesn't have any set, use the channel's. If the
			// channel doesn't, use the tune's. For now, just have the
			// tune's.

			if (t->type == CTT_ERROR)
				goto out;
			ed->notation = t->set_notation_sc;
			break;
		default: break;
	}
out:
	return r;
}

static int cmd_cd (struct cmd_t *t, char *end) {
	int r = 0;
	*t = cmd_t_get (t->e, end, 0, 0, CTGW_VALUE);
	char *buf = $put_t_in_scratch (t);

	// This malloc could be avoided. I won't call this fixme, because
	// who cares?
	char *full_path = get_full_path (buf);
	if (chdir (full_path))
		print_msg ("Can't cd to \"%s\"", full_path);
	free (full_path);
	$scratch_unget ();
	return r;
}

int run_cmd (struct line *line) {

	// Just ignore empty cmds
	if (is_all_space (line->n, line->d))
		return 1;
	int r = 0;
	char *end = line->d + line->n;
	auto t = cmd_t_get (line->d, end, CTC_N, cmd_strs, CTGW_CMD_SUBCMD);
	if (t.type != CTT_CMD_SC) {
		print_msg ("\"%.*s\": not a cmd", line->n, line->d);
		return 1;
	}
	switch (t.cmd) {
		case CTC_WRITE_QUIT:
			if (!cmd_write_export (&t, end, 0))
				exit (0);
			break;
		case CTC_QA:
		case CTC_QUIT:
			exit (0);
			break;
		case CTC_WRITE:
		case CTC_WA:
			cmd_write_export (&t, end, 0);
			break;
		case CTC_SCO:
			cmd_sco (&t, end);
			break;
		case CTC_SET:
			cmd_set (&t, end);
			break;
		case CTC_PLAY:
			if (0 == cmd_play (&t, end))
				ui.editor.ed.flags |= EF_COMPILED;
			break;
		case CTC_ORC:
			cmd_orc (&t, end);
			break;
		case CTC_CD:
			cmd_cd (&t, end);
			break;
		case CTC_PWD: {
			char buf[PATH_MAX];
			getcwd (buf, PATH_MAX);
			print_msg ("%s", buf);
		}
			break;
		default:
			$print_msg_get_out ("\"%.*s\" is not a cmd",
					(int) (t.e - t.s), t.s);
			break;
	}
out:
	return r;
}
