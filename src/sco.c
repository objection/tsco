#define _GNU_SOURCE

#include "sco.h"
#include "ui.h"

struct scos scos;
struct orcs orcs;

$ncmp_def_by_member_using_func (sco, path, char *, ncmp_strs);

int p_field_widths[PFT_N] = {
	[PFT_NOTE] = 3,
	[PFT_INSTR] = 2,
	[PFT_AMP] = 2,
	[PFT_P_FIELD] = 4,
};

// These are for read_sco_or_tsco.
static char *sco_path;
static int sco_line;

void free_event (struct event *event) {
	if (event->ps.d) free (event->ps.d);
}

// Note: doesn't set channel.n to 0
void free_channel (struct channel *channel) {
	$each (event, channel->d, channel->n)
		free_event (event);
}

// Note, this is skipblank, not skipnblank!
//
// Maybe I need to stop being so hilarious with my names. I just took
// this out and stuck cmd.c's skipnblank into misc.h, thinking
// skipnblank was shared between files. Whatever: it can stay there.
// I've no doubt I'll use that function in more places.

static void skipblank (char **_p) {
	char *p = *_p;
	while (*p && isblank (*p))
		p++;
	*_p = p;
}

// This operate on rows. It also shifts event->start by the same
// amount.

void shift_events (struct channel *channel, int from_row, int n_rows) {
	$each (event, channel->d, channel->n) {
		if (event->row >= from_row) {
			event->start += n_rows * $sco->row_len;
			event->row += n_rows;
		}
	}
}

// This function only makes sense if the array is sorted. Rather, if I
// end up with duplicate events -- events with the same instrument
// number and start time -- this will give you, obviously, just one of
// them. The array is qsorted every time you place a note. So it
// should be fine.

struct event *get_last_event (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	$flt max = 0;
	struct event *r = 0;
	$each (event, channel->d, channel->n) {
		if (
				// This >= means you'll get the last duplicate. Of course, there
				// should not be any duplicates.
				event->start >= max) {
			max = event->start;
			r = event;
		}
	}
	return r;
}

struct event *get_event_at_row (struct channel *channel,
		int row) {
	return arr_find (channel, &(struct event) {.row = row},
					sco_cmp_event_by_row);
}

// Takes a start time, not a row, so you have to remember to
// * it by sco->row_len.
struct event *get_next_event (int start_row) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	$each (event, channel->d, channel->n) {
		if (event->row > start_row) {
			return event;
		}
	}
	return 0;
}

struct event *get_prev_event (int start_row) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	if (channel->n)
		$beach (event, channel->d, channel->n) {
			if (event->row < start_row) {
				return event;
			}
		}
	return 0;
}

struct event sco_make_event (int instr, int channel, int row, $flt start) {
	struct event r = {
		.instr = instr, .start = start, .row = row, .dur = 0, .amp = 1.0,
	};
	return r;
}

int sco_cmp_event_by_row (const void *_a, const void *_b) {
	const struct event *a = _a;
	const struct event *b = _b;

   return a->row - b->row;
}

struct sco sco_make_default_sco (void) {
	struct sco r = {
		.dims = {.x = 6, .y = 500},
		.tempo = 120,
		.row_len = 1,
	};
	$fori (i, r.dims.x)
		arr_push (&r.channels, (struct channel) {0});
	return r;
}

static int p_field_compar_id (const void *_a, const void *_b) {
	const struct p_field *a = _a;
	const struct p_field *b = _b;
	return a->id - b->id;
}

static void write_i_statement (FILE *f, struct event *event, struct event *next) {
		fprintf (f, "i%d %f ",
				event->instr, event->start);

		// Trackers usually play the instruct until the next note
		// start. Csound requires that you say how long a note starts.
		// So just make the duration the time between this note's
		// start and the next's.

		// TODO: do something more sophisticated. Probably want to do
		// an automatic ramp. On the other hand, I don't want to do
		// really anything automatically. So maybe there's a better
		// solution.
		if (next)
			fprintf (f, "%f ", next->start - event->start);
		else
			fprintf (f, "5.0 "); // FIXME
		fprintf (f, "%f ", event->cps);
		fprintf (f, "%f ", event->amp);
		$qsort (event->ps.d, event->ps.n, p_field_compar_id);
		$each (event_p, event->ps.d, event->ps.n)
			fprintf (f, "%f ", event_p->val);
		fprintf (f, "\n");
}


// Need to write to a buffer because I also pass it
//
// TODO: decide whether to just keep this memory around. I can imagine
// you'll hit play all the time.
//
// TODO: add an events_changed flag. If you haven't changed an event,
// there's no reason to come here.
char *write_sco_to_buf (size_t *n_res) {
	struct sco *sco = &scos.d[scos.i];

	char *r = 0;

	// This isn't really a valid "n", as far I know. Check
	// open_memstream manpage.
	size_t buf_n;
	auto f = open_memstream (&r, &buf_n);
	char p_field[64];

#define $add(fmt, ...) \
	do { \
		sprintf (p_field, fmt, ##__VA_ARGS__); \
		fprintf (f, "%s", p_field); \
	} while (0)

	$add ("t 0 %f\n", sco->tempo);

	$each (channel, sco->channels.d, sco->channels.n) {
		$each (event, channel->d, channel->n) {
			struct event *next = event + 1 < channel->d + channel->n
				? event + 1
				: 0;

			write_i_statement (f, event, next);
		}
	}

	// *n_res includes the 0. You'd expect that, but I wasn't doing it
	// before.
	*n_res = buf_n; // CHECK: does this work?
	fclose (f);
	return r;
#undef $add
}

#define SCO_T \
	x(STT_ERROR, "bad token") \
	x(STT_I, "i") \
	x(STT_T, "t") \
	x(STT_NUM, "number") \
	x(STT_END, "end of file") \
	x(STT_NEWLINE, "newline") \
	x(STT_TSCO_OPENING_MARKUP, "T{") \
	x(STT_TSCO_CLOSING_MARKUP, "}") \
	x(STT_TSCO_KEYWORD, "tsco keyword") \
	x(STT_N, 0)
#define x(a, b) a,
enum sco_t_type {SCO_T};
#undef x
#define x(a, b) b,
char *sco_t_type_strs[] = {SCO_T};
#undef x

#define TSCO_KEYWORDS \
	x(TSCO_CHANNEL, "channel") \
	x(TSCO_N, 0)
#define x(a, b) a,
enum tsco_keyword {TSCO_KEYWORDS};
#undef x
#define x(a, b) b,
char *tsco_keyword_strs[] = {TSCO_KEYWORDS};
#undef x

struct sco_t {
	char *s, *e;
	enum sco_t_type type;
	enum tsco_keyword tsco_keyword;
} static t;

static void get_sco_t (bool is_tsco) {
	t.s = t.e;
	skipblank (&t.s);
	t.e = t.s + 1;
	switch (*t.s) {
		case 0:
			t.type = STT_END;
			t.e = t.s;
			break;
		case '\n':
			t.type = STT_NEWLINE;
			break;
		case 'i':
			t.type = STT_I;
			break;
		case 't':
			t.type = STT_T;
			break;
		case '.':
		case '0' ... '9':
			while (*t.e == '.' || isdigit (*t.e)) t.e++;
			t.type = STT_NUM;
			break;
		default:
			if (is_tsco) {
				if (*t.s == 'T' && *t.e == '{') {
					t.e++;
					t.type = STT_TSCO_OPENING_MARKUP;
					break;
				}
				if (*t.s == '}') {
					t.type = STT_TSCO_CLOSING_MARKUP;
					break;
				}
				while (*t.e && !isspace (*t.e))
					t.e++;
				if ($strnmatch (t.s, tsco_keyword_strs[TSCO_CHANNEL],
							t.e - t.s)) {
					t.type = STT_TSCO_KEYWORD;
					t.tsco_keyword = TSCO_CHANNEL;
				} else
					t.type = STT_ERROR;
			}
			break;
	}
}

// These macros are all a little flimsy. They assume that the function
// that's calling them have "path" and "line" defined, and they're
// called that and not pointers.
#define $get() \
	get_sco_t (is_tsco)
#define $parse_err(_fmt, ...) \
	do { \
		$print_msg_get_out ("%s:%d: " _fmt, sco_path, sco_line, ##__VA_ARGS__); \
	} while (0)
#define $expect(_type) \
	({ \
		$get (); \
		if (t.type != _type) \
			$parse_err ("expected %s but found %s", \
					sco_t_type_strs[_type], sco_t_type_strs[t.type]); \
	})
#define $parse_int(_val) \
	if (1 != sscanf (t.s, "%d", _val)) \
		$parse_err ("\"%.*s\" isn't an integer", (int) (t.e - t.s), \
				t.s);
#define $parse_double(_val) \
	if (1 != sscanf (t.s, "%lf", _val)) \
		$parse_err ("\"%.*s\" isn't a double", (int) (t.e - t.s), \
				t.s);

static int parse_i_inner (struct event *retval, bool is_tsco) {
	int r = 0;
	// p1, instr
	$expect (STT_NUM);
	$parse_int (&retval->instr);

	// p2, start
	$expect (STT_NUM);
	$parse_double (&retval->start);

	// p3, dur
	$expect (STT_NUM);
	$parse_double (&retval->dur);

	// p4, assumes freq
	$expect (STT_NUM);
	$parse_double (&retval->cps);

	// p5, assumes amp
	$expect (STT_NUM);
	$parse_double (&retval->amp);

	double val;
	// Rest of the ps
	int id = 0;
	while ($get (), t.type == STT_NUM) {
		$parse_double (&val);
		arr_push (&retval->ps, ((struct p_field) {.val = val, .id = id++}));
	}
out:
	return r;
}

static int parse_i (struct sco *sco, bool is_tsco, int channel_i) {
	int r = 0;
	struct event event = {0};
	if (parse_i_inner (&event, is_tsco))
		return 1;

	if (is_tsco) {
		struct channel *channel = &sco->channels.d[channel_i];
		arr_push (channel, event);
		if (t.type == STT_END)
			goto out;
		else if (t.type == STT_NEWLINE)
			sco_line++;
		else
			$parse_err ("\"%s\": statements must end with newline or EOF");
	} else {
		assert (!"Not implemented");
	}
out:
	return r;
}

// r: the 4 in T{channel 4}.
// Just parses the middle channel and the 4.
static int parse_channel (int *retval) {
	bool is_tsco = 1;
	int r = 0;
	$expect (STT_NUM);
	$parse_int (retval);
	$expect (STT_TSCO_CLOSING_MARKUP);
out:
	return r;
}

int read_sco_or_tsco (struct sco *sco, char *path, bool is_tsco) {
	int r = 0;
	char *buf;

	// FIXME: properly calculate the size of the sco
	*sco = sco_make_default_sco ();
	if (!(buf = $slurp_od (path)))
		return 1;

	// File-locals
	sco_line = 1;
	sco_path = path;

	t = (struct sco_t) {.e = buf};

	// Just run through the whole to get the number of channels, then
	// allocate them.
	if (is_tsco) {
		int max_channel = 0;
		while ($get (), t.type != STT_END) {
			if (t.type == STT_TSCO_OPENING_MARKUP) {
				$expect (STT_TSCO_KEYWORD);
				switch (t.tsco_keyword) {
					case TSCO_CHANNEL: {
						int channel = 0;
						parse_channel (&channel);
						max_channel = $max (channel, max_channel);
					}
						break;
					default:
						$print_msg_get_out ("\"%.*s\" is not a tsco keyword",
								(int) (t.e - t.s), t.s);
				}
			}
		}
		$fori (i, max_channel + 1)
			arr_push (&sco->channels, (struct channel) {0});
	}


	t.e = buf;
	while ($get (), t.type != STT_END) {
		switch (t.type) {
			case STT_NEWLINE:
				sco_line++;
				break;
			case STT_ERROR:
				$parse_err ("\"%.*s\": invalid token", (int) (t.e - t.s), t.s);
				break;
			case STT_I:
				if (is_tsco)
					$parse_err ("\
tsco files must have, eg T{\"%s 3}\" in front of their i statements",
							tsco_keyword_strs[TSCO_CHANNEL]);
				parse_i (sco, is_tsco, -1);
				break;
			case STT_T:
				$expect (STT_NUM);
				// FIXME: I'm assuming right now that your tempo statement looks
				// like this: "t 0 120". But tempo statements can look like:
				// "t 0 120 55 340 10 44".
				$expect (STT_NUM);
				sco->tempo = 0;
				break;
			case STT_TSCO_OPENING_MARKUP:
				$expect (STT_TSCO_KEYWORD);
				switch (t.tsco_keyword) {
					case TSCO_CHANNEL: {
						int channel = 0;
						parse_channel (&channel);

						// FIXME: I'm not distinguishing between ints and floats. This
						// means if somehow you had a $flt channel or instr number, bad
						// things might happen.

						$expect (STT_I);
						parse_i (sco, is_tsco, channel);
					}
						break;
					default:
						$print_msg_get_out ("\"%s\" is not a tsco keyword",
								(int) (t.e - t.s), t.s);
				}

				break;
			default:
				$parse_err ("\"%s\" isn't allowed here", sco_t_type_strs[t.type]);
				break;
		}
	}

out:
	free (buf);
	return r;
}



int write_tsco (struct sco *sco, char *path) {
	if (fexists (path)) {
		if (!$find (path, &scos.d, scos.n, ncmp_scos_by_path_using_strcmp)) {
			print_msg ("\
Writing to \"%s\" would overwrite an already-existing file");
			return 1;
		}
	}
	FILE *f = fopen (path, "w");
	if (!f) {
		print_msg ("Couldn't open %s: %s", path, strerror (errno));
		return 1;
	}

#define $add(text, ...) \
	do { fprintf (f, text, ##__VA_ARGS__); } while (0)

	$add ("t 0 %f\n", sco->tempo);

	$each (channel, sco->channels.d, sco->channels.n) {

		// Right now and hopefully forever, the only difference
		// between a sco and a tsco is this "tsco-channel %zu" that's
		// prepended to each i statement. tsco (the program) can't
		// just read a sco file because sco files have no concept of a
		// "channel" or "track". I want the differences to be utterly
		// minimal. I put it at the start of the statement and don't
		// do something like
		//
		// 		channel 1
		// 			14 ...
		//
		// because I want csound to sort it.
		//
		// You should be able to sed 's/tsco-channel %zu://' to turn a
		// tsco into a sco.
		$each (event, channel->d, channel->n) {
			$add ("T{channel %zu} ", channel - sco->channels.d);
			struct event *next = event + 1 < channel->d + channel->n
				? event + 1
				: 0;
			write_i_statement (f, event, next);
		}
	}

	fclose (f);

#undef $add
	return 0;
}

