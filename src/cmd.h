#pragma once

#include "types.h"

$make_arr (char, line);
$make_arr_with_member (struct line, history, struct {int history_search_x; int y;

		// FIXME: "name" shouldn't be here. I feel like it should be
		// part of the "buffer".
		int matched_line; int x; char *name;});

struct cl {
	struct history history;
	struct history pfield_history;
	struct history *current_history;
	struct strs errors;
	int last_line_search; // 0 for no search
	int last_line_search_dir;
	bool was_to;
	int window_height;
};


int run_cmd (struct line *line);
void free_line (struct line *line);
int start_play (char *sco_path, char *orc_path);

