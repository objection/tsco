#define _GNU_SOURCE

#include "types.h"
#include "cmd.h"
#include "ui.h"
#include "input.h"
#include "sco.h"

static struct args {
} args;

static error_t parse_opt (int key, char *arg, struct argp_state *state) {

	switch (key) {
		case 'o':

			// It's an error to open an orc that doesn't exist,
			// because you can't edit them in tsco.
			if (!fexists (arg))
				errx (1, "orc %s doesn't exist", arg);
			arr_push (&orcs, ((struct orc) {.path = arg, .instr = 1}));
			break;
		case ARGP_KEY_INIT:
			break;
		case ARGP_KEY_ARG:
			arr_push (&scos, (struct sco) {.path = arg});
			break;
		case ARGP_KEY_END:
			break;
		default: break;
	}
	return 0;
}

static void tsco_exit (void) {
	endwin ();
}

static void init_ncurses (void) {
	initscr ();
	raw ();
	noecho ();
	keypad (stdscr, 1);
#if 0
	if (start_color ()) err (1, "start_color failed");

	if (!has_colors ()) errx (1, "Terminal doesn't support colours\n");

	init_pair (1, COLOR_BLACK, COLOR_RED);
	init_pair (2, COLOR_RED, COLOR_BLACK);
	init_pair (3, COLOR_GREEN, COLOR_BLACK);
	init_pair (4, COLOR_YELLOW, COLOR_BLACK);
	init_pair (5, COLOR_BLUE, COLOR_BLACK);
	init_pair (6, COLOR_MAGENTA, COLOR_BLACK);
	init_pair (7, COLOR_CYAN, COLOR_BLACK);
	init_pair (8, COLOR_WHITE, COLOR_BLACK);
#endif
	atexit (tsco_exit);
}

static void update (void) {
	struct sco *sco = &scos.d[scos.i];

	// Just do this every time. I'm really starting to think small
	// inefficiencies are worth it when they make things less
	// cluttered.

	sco->curs.x = $clamp (sco->curs.x, 0, sco->channels.n- 1);
	sco->curs.y = $clamp (sco->curs.y, 0, sco->dims.h - 1);
}


static uintptr_t cs_thread (void *user) {

	// TODO: Not really a todo. Pass ui, or whatever through user if
	// need be.

	int r = 0;
	struct ed *ed = &ui.editor.ed;
	FILE *f = fopen ("/tmp/out", "w");
	if (!f)
		err (1, "couldn't open %s", "/tmp/out");
	int n_msgs;
	int n_reps = 0;
	struct timespec sleep_time = {
		.tv_sec = 0, .tv_nsec = 1000000000 / 30,
	};
	while (1) {
		while (!(ed->flags & EF_COMPILED))
			nanosleep (&sleep_time, 0);
		ed->flags |= EF_PLAYING;
		while ((ed->flags & EF_PLAYING)
				// FIXME: use csoundPerformKsmpsAbsolute when it
				// comes to Fedora. I think it'll let me play keys
				// over the music.
				&& 0 == csoundPerformKsmps (cs)) {

			// Use n_reps to keep from calling csoundGetMessageCnt
			// over and over. An alternative is using csound message
			// callback functions.

			if (n_reps > 10 && (n_msgs = csoundGetMessageCnt (cs))) {
				$fori (i, n_msgs) {
					const char *msg = csoundGetFirstMessage (cs);
					fprintf (f, "%s\n", msg);
					csoundPopFirstMessage (cs);
				}
				n_reps = 0;
			}
			n_reps++;
		}

		// FIXME: csound doesn't seem to want to reuse a message
		// buffer, and there's no function to say "rewind the buffer",
		// so I have to destroy it here. It's created in cmd_play.

		csoundDestroyMessageBuffer (cs);
		csoundReset (cs);
		ed->flags &= ~EF_PLAYING;
		// FIXME: EF_COMPILED is pointless, the way the code is right now.
		ed->flags &= ~EF_COMPILED;
	}
	fclose (f);
	return r;
}

static void init_csound (void) {
	cs = csoundCreate (0);
	// Csound encounters some sort of error and doesn't reset errno.
	errno = 0;
}

int main (int argc, char **argv) {
	// You have to do this to use Unicode. No doubt the reason for
	// this is in the manpage. There's an explanation here:
	// http://dillingers.com/blog/2014/08/10/ncursesw-and-unicode/
	//
	// Some of what it says seems to be out of day. For example, I
	// don't seem to need to include ncursesw/ncurses.
	char *locale_string = setlocale (LC_ALL, "");
	if (!locale_string)
		err (1, "couldn't set locale");

	struct argp argp = {
		.options = (struct argp_option []) {
			{"orc", 'o', "PATH.orc", 0,
				"Load a .orc file. You need these, though you \
can add them later (\"orc add file.orc\")"},
			{0},
		},
		parse_opt,
		"file_1.sco file_2.sco ...", "\
A tracker for writing csound score files ...\v\
Blah blah"
	};

	// Maybe use this. The API examples do
	/* csoundInitialize (CSOUNDINIT_NO_ATEXIT); */
	/* CSOUND *cs = csoundCreate (0); */
	/* int ret = csoundReadScore (cs, 0); */
	argp_parse (&argp, argc, argv, 0, 0, NULL);

	scratch = scratch_create (0xfff * 3);

	// Perhaps should load all scos here.
	if (scos.n)
		read_sco_or_tsco (&scos.d[0], scos.d[0].path, 1);
	else
		arr_push (&scos, sco_make_default_sco ());
	init_csound ();
	init_ncurses ();
	init_ui ();


	// NOTE: csoundCreateThread returns an "opaque pointer that
	// represents the thread". I don't really need it. Truthfully, I'm
	// not really sure what to do with it. I think _join. But I won't
	// be joining. The thread runs until the program exits.
	//
	// But I need to hang on to it to please libasan.
	void *csound_thread;
	if (!(csound_thread = csoundCreateThread (cs_thread, 0)))
		err (1, "couldn't create csound thread");

	while (1) {
		// TODO: only set this when ncurses reports change.
		getmaxyx (stdscr, ui.dims.h, ui.dims.w);
		input ();
		update ();
		ui.changed = 1;
	}

	/* csoundDestroyMessageBuffer (cs) */
	csoundDestroy (cs);
	exit (0);
}



