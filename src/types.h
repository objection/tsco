#pragma once

// I call this "types", but it's really "everything." Everything
// that's used everywhere.

#define _XOPEN_SOURCE_EXTENDED
#include "../lib/darr/darr.h"
#include "../lib/frequency-calculator/frequency-calculator.h"
#include "../lib/n-fs/n-fs.h"
#include "../lib/slurp/slurp.h"
#include "../lib/useful//useful.h"
#include <argp.h>
#include <assert.h>
#include <csound/cscore.h>
#include <csound/csound.h>
#include <ctype.h>
#include <err.h>
#include <float.h>
#include <fnmatch.h>
#include <locale.h>
#include "../lib/ncmp/ncmp.h"
#include <ncurses.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#define $n_default_ps 3
#define $flt MYFLT
#define $max_octave 15
#define $min_octave 0
#define $c4 261.63

// I'm using global variables everywhere in this code. I don't know if
// it's bad. I mean, I feel the howls of laughter from the depths of
// my paranoia. But whatever. One problem with bit global structs like
// this is the first thing you do when writing a function is go
// "struct my_member *my_member = &my.global.struct; So let's just
// make some macros, shall we?
#define $curhist (ui.cmdline.cl.current_history)
#define $sco (&scos.d[scos.i])
#define $channel (&$sco->channels.d[$sco->curs.x])
#define $c2t(_y) \
	((_y) * $sco->row_len)

#define $slurp_od(path) \
	({ \
		char *_res = slurp (path); \
		if (!_res) \
			print_msg ("Couldn't read %s: %s", path, strerror (errno)); \
		_res; \
	})
#define $tsco_go() \
	do { r = 1; goto out; } while (0)
#define $print_msg_get_out(fmt, ...) \
	 do { print_msg (fmt, ##__VA_ARGS__); $tsco_go (); } while (0)

#define $scratch_get(amount) ({ scratch_get (&scratch, amount); })
#define $scratch_unget() \
	({ \
	 	if (!scratch.starts.n) \
			assert ("you've done too many ungets"); \
		scratch_unget (&scratch); \
	})

// $get_whatever_and_get_out.
#define $get_num_or_get_out_inner(_val, _p, _val_name, _range_s, _range_e, _fmt, \
		_tmpval) \
		if (1 != sscanf (_p, _fmt, _tmpval)) \
			$print_msg_get_out ("%s is not an int", _tmpval); \
		/* _range_s == -1 && _range_e == -1 to not to a range check */ \
		if (_range_s != -1 && _range_e != -1 \
				&& (*_tmpval < _range_s || *_tmpval > _range_e)) \
			$print_msg_get_out ("%s must be between %d and %d", \
				_val_name, _range_s, _range_e); \
		*_val = *_tmpval; \

#define $get_int_or_get_out(_val, _p, _val_name, _range_s, _range_e) \
	do { \
		int tmpval = 0; \
		$get_num_or_get_out_inner (_val, _p, _val_name, _range_s, _range_e, "%d", \
				&tmpval); \
	} while (0)

#define $get_hex_or_get_out(_val, _p, _val_name, _range_s, _range_e) \
	do { \
		int tmpval = 0; \
		$get_num_or_get_out_inner (_val, _p, _val_name, _range_s, _range_e, "%x", \
				&tmpval); \
	} while (0)

#define $get_flt_or_get_out(_val, _p, _val_name, _range_s, _range_e) \
	do { \
		$flt tmpval = 0; \
		$get_num_or_get_out_inner (_val, _p, _val_name, _range_s, _range_e, "%lf", \
				&tmpval); \
	} while (0);

$make_arr (char, str);
$make_arr (char *, strs);
$make_arr ($flt, doubles);

struct v {
	union {
		struct {
			int x, y;
		};
		int arr[2];
		struct {
			int w, h;
		};
	};
};

struct range {

	// Note that the range is in rows, not event start times.
	int start, end;
	bool failed;

	// This "special_addition" is my solution to the the fact that e
	// by itself behaves differently to, eg, de. I might be missing
	// something.
	//
	// But, it's like this. Put your cursor on the "V" in VIM, then
	// press e. You end up on M. Now do de. You delete the e.
	//
	// This is different to how, eg, w behaves. If you put your cursor
	// somewhere in the "HELLO" in HELLO THERE and press w, you end up
	// on T. But if you do dw, you do _not_ delete the T.
	//
	// There's something about this somewhere in the help, but I can't
	// find it. It makes perfect sense that e does delete the ...
	//
	// Wait a a minute. If I imagine the deletion as inclusively
	// deleting the range, it works. No, it doesn't. Because then w
	// would delete the first character of the next word. Then how
	// about we say that w merely goes to the character before the
	// next word? But it doesn't.
	//
	// So, it's a special case. And that's what this is.
	$flt special_addition;
};


extern struct scratch scratch;
extern CSOUND *cs;
