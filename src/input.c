#define _GNU_SOURCE

#include "cmd.h"
#include "input.h"
#include "ui.h"
#include "misc.h"
#include "sco.h"
#define $ctrl(x) ((x) & 0x1f)

enum ch_state {
	GCF_GET,
	GCF_RECORD,
	GCF_PLAY
};

// A macro is just a string with a i member. It's a circular queue,
// perhaps. See set_ch, case GCF_PLAY.
$make_arr_with_member (char, macro, int i);

struct chget {
	enum ch_state state;
	int ch;
	bool swallow;
	struct macro macro;
} static chget;

#if 0
bool confirm (char *fmt, ...) {
	assert (ui_initialised);
	va_list ap;
	va_start (ap, fmt);
	char buf[PATH_MAX];
	vsnprintf (buf, PATH_MAX, fmt, ap);
	/* print_msg (); */

	va_end (ap);
}
#endif

void set_ch (WINDOW *win) {
	if (chget.swallow) {
		chget.swallow = 0;
		return;
	}
	// FIXME: As usual, stick this in three functions if I can be arsed.
	switch (chget.state) {
		case GCF_GET:
			chget.ch = wgetch (win);
			break;
		case GCF_RECORD:
			chget.ch = wgetch (win);
			arr_push (&chget.macro, chget.ch);
			break;
		case GCF_PLAY:
			assert (chget.macro.n);
			if (chget.macro.i == chget.macro.n)
				chget.macro.i = 0;
			chget.ch = chget.macro.d[chget.macro.i++];
			break;
	}
}

void hit_enter (void) {
	auto win = ui.editor.win;
	auto sco = &scos.d[scos.i];
	auto channel = &sco->channels.d[sco->curs.x];

	while (1) {
		struct event *event = 0;
		set_ch (win);
		switch (chget.ch) {
			case KEY_RESIZE:
				ui.cmdline_window.dims.y = msgs.n;
				resize ();
				break;
			case $ctrl ('l'):
				clear_all ();
				refresh_all ();
				break;
			case 27:
			case $ctrl ('c'):
			case $ctrl ('@'):
				ui.mode = M_NORMAL;
				print_msg ("");
				goto out;
			default:
				break;
		}
		ui.changed = 1;

		if (ui.mode == M_REPLACE_ONE) {
			if (chget.state != GCF_GET)
				$sco->curs.y++;
			ui.mode = M_NORMAL;
			break;
		}
	}
	out:
	return;
}

// FIXME: get rid of this function. Every function here that's not
// input_normal should be called from within input_normal. See
// /repeat_insert/ for more explanation.
void input (void) {
	switch (ui.mode) {
		case M_NORMAL:
			input_normal ();
			break;
		case M_INSERT:
		case M_REPLACE:
		case M_REPLACE_ONE:
			input_insert ();
			break;
		case M_CMDLINE_WINDOW_INSERT:
		case M_CMDLINE_WINDOW_REPLACE:
		case M_CMDLINE_WINDOW_REPLACE_ONE:
		case M_PROMPT:
		case M_INSTR_INSERT:
		case M_AMP_INSERT:
		case M_PFIELD_INSERT:
			input_text_insert ();
			break;
		case M_CMDLINE_WINDOW_NORMAL:
			input_cmdline_normal ();
			break;
		case M_HIT_ENTER:
			input_hit_enter ();
			break;
		default:
			break;
	}
}

static void print_insert (void) {
	print_msg ("-- INSERT --");
}

static void print_replace (void) {
	print_msg ("-- REPLACE --");
}

static struct event *normal_caret (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	if (0 == channel->n)
		return 0;
	struct event *r = &channel->d[0];
	return r;
}

static void straight_scroll_down (int *pad_start_y, int *curs_y, int *pad_height,
		int amount) {
	*pad_start_y += amount;
	if (*pad_start_y > *pad_height - amount)
		*pad_start_y = *pad_height - amount;
	if (*curs_y < *pad_start_y)
		*curs_y = *pad_start_y;
}

static void straight_scroll_up (int *pad_start_y, int *curs_y, int *pad_height,
		int amount) {
	*pad_start_y -= amount;
	if (*pad_start_y < 0)
		*pad_start_y = 0;
	if (*curs_y > *pad_start_y + *pad_height - amount)
		*curs_y = *pad_start_y + *pad_height - amount;
}

// TODO (possible): collapse ctrl_n and ctrl_p together.
static int ctrl_p (void) {
	auto history = ui.cmdline.cl.current_history;
	if (history->n <= 0 || history->y != history->n - 1)
		return 1;
	auto prompt = &history->d[history->n - 1];
	int len;

	// When you do keep doing ctrl-p, you keep getting matches based
	// on your initial cursor position. So I hang on to that, in
	// history->completion_x. I set it to -1 when you stop completing,
	// and set it to history->x here if it's -1, ie, you've just
	// started completion.
	if (history->history_search_x == -1) {
		len = history->history_search_x = history->x;
		history->matched_line = history->n - 1;
	} else
		len = history->history_search_x;

	$forri (i, history->matched_line - 1) {
		if (!history->d[i].d)
			continue;
		if ($strnmatch (prompt->d, history->d[i].d, len)) {

			// Copy the match string into history->d[history->n - 1].
			arr_reserve (prompt, history->d[i].n);
			memcpy (prompt->d, history->d[i].d, history->d[i].n);
			prompt->n = history->d[i].n;
			history->matched_line = i;

			// When Vim gets a match, it puts your cursor on the
			// prompt line
			history->x = history->d[i].n;
			break;
		}
	}
	return 0;
}

// TODO (possible): collapse ctrl_n and ctrl_p together. I'm less and
// less interested in doing those things. You think, well, keeping it
// all together will help reduce inconsistencies (bugs), but you spend
// so long fucking with it -- functions pointers -- that I dunno if
// it's worth it.
static int ctrl_n (void) {
	struct history *history = ui.cmdline.cl.current_history;
	if (history->n <= 0 || history->y != history->n - 1)
		return 1;
	struct line *prompt = &history->d[history->n - 1];
	int len;

	// When you do keep doing ctrl-p, you keep getting matches based
	// on your initial cursor position. So I hang on to that, in
	// history->completion_x. I set it to -1 when you stop completing,
	// and set it to history->x here if it's -1, ie, you've just
	// started completion.
	if (history->history_search_x == -1) {
		len = history->history_search_x = history->x;
		history->matched_line = 0;
	} else
		len = history->history_search_x;
	bool got = 0;

	for (int i = history->matched_line + 1; i < history->n - 1; i++) {
		if ($strnmatch (prompt->d, history->d[i].d, len)) {
			arr_reserve (prompt, history->d[i].n);
			memcpy (prompt->d, history->d[i].d, history->d[i].n);
			history->matched_line = i;

			// When Vim gets a match, it puts your cursor on the
			// prompt line
			history->x = history->d[i].n;
			break;
		}
	}
	if (!got) {
		prompt->n = len;
		history->x = prompt->n;
		history->matched_line = history->n - 1;
	}
	return 0;
}

#if 0
static int cmp_line (const void *_a, const void *_b) {
	const struct line *a = _a;
	const struct line *b = _b;
	int r = 0;
	if ((r = a->n - b->n) != 0)
		return r;
	if (!a->d || !b->d)
		return a->d - b->d;
	else
		// We already know they're the same length
		return (strncmp (a->d, b->d, a->n));
}
#endif

static char *check_skip (void) {
	auto event = get_event_at_row ($channel, $sco->curs.y);
	if (!event || !$curhist->n)
		return 0;
	auto last_line = &$curhist->d[$curhist->n - 1];
	char *r = get_skipnblanked (last_line->n, last_line->d);
	if (r == last_line->d + last_line->n)
		return 0;
	return r;
}

static int set_instr (void) {
	char *p = check_skip ();
	if (!p)
		return 0;

	int r = 0;
	auto event = get_event_at_row ($channel, $sco->curs.y);
	if (isdigit (*p)) {
		$get_hex_or_get_out (&event->instr, p,
				"instr", 0,
				// FIXME: this perhaps should be the max instruments.
				// I think I've talked about this elsewhere. Basically
				// I'm not sure if I should make you only use a valid
				// instr. Because I can see you wanting to just put
				// down a bunch of notes and sort out the orc later.
				// Probably want to make a $get_int_or_get_out that
				// warns but accepts the value.
				0xff);
	} else {
		print_msg ("Not put in named p-fields yet");
	}
out:
	return r;

}

// Identical to set_instr, except it accepts doubles, not ints. Well,
// that might change. It also does amps, so there's special-casing.
static int set_p_field (void) {
	char *p = check_skip ();
	if (!p)
		return 0;
	int r = 0;
	struct event *event = get_event_at_row ($channel, $sco->curs.y);
	char name[16];
	double *val;
	// This is hard-coded. 2 is amp.
	if ($sco->curs_p_field == 2) {
		snprintf (name, 15, "amp");
		val = &event->amp;
	} else {
		snprintf (name, 15, "p%d", $sco->curs_p_field);
		int n_default_ps = 3; // i, note, and amp.
		arr_reserve (&event->ps, $sco->curs_p_field - n_default_ps
				// Just allocate a bit extra.
				* 2);
		val = &event->ps.d[$sco->curs_p_field - n_default_ps].val;
	}

	if (isdigit (*p)) {
		$get_flt_or_get_out (val, p,
				// FIXME: find some good, sane values for min/max
				// range?
				name, 0, 1);
	} else
		print_msg ("Not put in named p-fields yet");
out:
	return r;
}

// Returns 1 if there's no cmd to run.
static int cmdline_return (struct history *history, bool _run_cmd) {
	auto current = &history->d[history->y];
	if (_run_cmd) {
		switch (ui.mode) {
			case M_PROMPT:
				run_cmd (current);
				break;
			case M_CMDLINE_WINDOW_NORMAL:
			case M_CMDLINE_WINDOW_INSERT:
				run_cmd (current);
				break;
			case M_INSTR_INSERT:
				set_instr ();
				break;
			case M_AMP_INSERT:
			case M_PFIELD_INSERT:
				set_p_field ();
				break;
			default:
				break;
		}
	}
	ui.mode = M_NORMAL;

	// If you've done a ctrl-p, your history->y (the current line)
	// will be one in the history, so stick it to the end of the list
	// and remove it.
	struct line newish = {};

	current->n = strip (current->d, current->n);

	// I really seem to need to make a new object, here. Why? Why? I
	// can't just shove it on the end. I don't get it.
	arr_insert_mul (&newish, 0, current->d, current->n);
	free (current->d);
	arr_splice (history, history->y, 1);
	if (current->n) {
		arr_push (history, newish);

		// FIXME: do this. I can't get it to work and it's not
		// a priority.
		// 		Get rid of the line you're matching on. Eg:
		//
		// 			echo<c-p>
		//
		// 		Get rid of the one that says echo
	// Stick a new, blank one on to the end. This is what'll be used
	// when you next type : (history.d[history.y]).
		arr_push (history, (struct line) {});
	}
	history->y = history->n - 1;
	history->x = 0;
	return 0;
}

static bool iskeyword (char the_char) {
	return isalnum (the_char) || the_char == '_';
}

static int cmdline_caret (void) {
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	if (!current->n)
		return history->x;
	int x = 0;
	while (x < current->n
			&& isblank (current->d[x]))
		x++;
	return x;
}

static int mvnt_w (int x) {
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	if (!current->d)
		return x;
	char *p = &current->d[x];
	char *end = current->d + current->n;
	if (ispunct (*p)) {
		while (p < end && ispunct (*p))
			p++;
	} else if (iskeyword (*p)) {
		while (p < end && (iskeyword (*p)))
			p++;
	}

	// NOTE: I'm leaving you at the end of the line. This is what zsh
	// does. In vim, when you're in "commandline mode", you're in the
	// cmdline window (which this editor will probably have to), your
	// cursor will go to the next line.
	while (p < end && (*p == 0 || isspace (*p)))
		p++;
	return p - current->d;
}

static int mvnt_e (int x) {
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	if (!current->d)
		return x;
	char *p = &current->d[x];
	char *end = current->d + current->n;
	if ((ispunct (*p) || iskeyword (*p)) && isspace (*(p + 1)))
		p++;

	while (isspace (*p)) p++;
	if (ispunct (*p)) {
		while (p < end && ispunct (*p))
			p++;
		p--;
	} else if (iskeyword (*p)) {
		while (p < end && (iskeyword (*p)))
			p++;
		p--;
	}
	return p - current->d;
}

static int mvnt_W (int x) {
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	if (!current->d)
		return x;
	char *p = &current->d[x];
	char *end = current->d + current->n;
	while (p < end && isblank (*p)) p++;
	while (p < end && !isblank (*p)) p++;
	while (p < end && isblank (*p)) p++;

	return x = p - current->d;
}

static int mvnt_E (int x) {
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	if (!current->d)
		return x;
	char *p = &current->d[x];
	char *end = current->d + current->n;
	if (!isblank (*p) && isblank (*(p + 1)))
		p++;
	while (p < end && isblank (*p)) p++;
	while (p < end && !isblank (*p)) p++;
	p--;
	return x = p - current->d;
}

static int mvnt_B (int x) {
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	if (!current->d)
		return x;
	char *p = &current->d[x];
	if (p > current->d && !isblank (*p) && isblank (*(p - 1)))
		p--;
	while (p > current->d && isblank (*p)) p--;
	while (p > current->d && !isblank (*p)) p--;
	if (p != current->d) p++;
	return p - current->d;
}

static int mvnt_b (int x) {
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	if (!current->d || x == 0)
		return x;
	char *p = &current->d[x];

	// A bit torturous, I know.
	if (ispunct (*p) && !ispunct (*(p - 1)))
		p--;
	else if (iskeyword (*p) && !iskeyword (*(p - 1)))
		p--;
	while (p > current->d && (*p == 0 || isspace (*p)))
		p--;
	if (ispunct (*p)) {
		while (p > current->d && ispunct (*p))
			p--;
	} else if (iskeyword (*p)) {
		while (p > current->d && (iskeyword (*p)))
			p--;
	}
	if (p != current->d) p++;
	return p - current->d;
}

static int mvnt_ge (int x) {
	return mvnt_e (mvnt_b (mvnt_b (x)));
}

static int mvnt_gE (int x) {

	// This is fun. I don't get to chain functions like this int C
	// very often. That's my own fault. Maybe I'll start writing more
	// functionally.
	return mvnt_E (mvnt_B (mvnt_B (x)));
}

static int search_char_in_line_fw (int ch) {
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	if (!current->n)
		return history->x;
	int x = history->x;
	if (current->d[x] == ch)
		x++;
	char *p = memchr (&current->d[x], ch, current->n - x);
	if (p)
		return p - current->d;
	return history->x;
}

static int search_char_in_line_bw (int ch) {
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	if (!current->n)
		return 0;
	int x = history->x;
	if (current->d[x] == ch)
		x--;
	char *p = memrchr (current->d, ch, x);
	if (p) {
		return p - current->d;
	}
	return 0;
}

static int cmdline_f (void) {
	struct cl *cl = &ui.cmdline.cl;
	set_ch (ui.cmdline.win);
	int r = search_char_in_line_fw (chget.ch);
	cl->last_line_search = chget.ch;
	cl->last_line_search_dir = 1;
	return r;
}

static int cmdline_F (void) {
	struct cl *cl = &ui.cmdline.cl;
	set_ch (ui.cmdline.win);
	int r = search_char_in_line_bw (chget.ch);
	cl->last_line_search = chget.ch;
	cl->last_line_search_dir = -1;
	return r;
}

//
// FIXME: this function's got a lot of special cases. It means I'm
// missing the point. There's a cleverer way of doing it.
//
// Make it work the same as normal_d. And then, inevitably, make it
// faux object-orientated, so there's just a "d" function, which calls
// functions based on the mode. Or not. It's a complete fucking waste
// of time. But it would keep everything consistent.
//
static void cmdline_d (void) {
	WINDOW *win = ui.cmdline.win;
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	set_ch (win);
	int mark_end, mark_start;
	switch (chget.ch) {
		case 'd':
			current->n = history->x = 0;
			break;
		case '$':
			current->n = history->x;
			break;
		case 'e':
			mark_end = mvnt_e (history->x) + 1;
			mark_start = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			break;
		case 'w':
			mark_end = mvnt_w (history->x);
			mark_start = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			break;
		case 'b':
			mark_start = mvnt_b (history->x);
			mark_end = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			// FIXME: dodgy
			history->x = mark_start;
			break;
		case 'f':
			// FIXME: this + 1 is dodgy. At least, I don't understand
			// why I need it.
			mark_end = cmdline_f () + 1;
			mark_start = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			break;
		case 'F':
			mark_start = cmdline_F ();
			mark_end = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			// FIXME: dodgy
			history->x = mark_start;
			break;
		case 't':
			mark_end = history->x;
			mark_start = cmdline_f ();
			arr_splice (current, mark_start, mark_end - mark_start);
			break;
		case 'T':
			mark_start = cmdline_F ();
			mark_end = history->x;
			arr_splice (current, mark_start, mark_end - mark_start);
			break;
		default:
			break;
	}
}

// FIXME: not tested.
static void resize (void) {
	getmaxyx (stdscr, ui.dims.y, ui.dims.x);
	ui.changed = 1;
	refresh_all ();
}

static int y_to_pad_line (int y, int pad_height) {
	struct history *history = ui.cmdline.cl.current_history;
	return pad_height - history->n + history->y;
}

static int pad_line_to_y (int pad_line, int pad_height) {
	// The pad line is 994 and the history are 10,
	// the line is 4.
	int r = pad_height - pad_line;
	return r;
}

static void keep_x_within_range (void) {
	struct history *history = ui.cmdline.cl.current_history;
	struct line *current = &history->d[history->y];
	if (history->y < 0)
		history->x = 0;
	else if (!current->n || history->x > current->n)
		history->x = current->n;
}

void input_cmdline_normal (void) {
	WINDOW *win = ui.cmdline.win;
	struct window *window = &ui.cmdline_window;
	struct cl *cl = &ui.cmdline.cl;
	struct history *history = ui.cmdline.cl.current_history;

	// If your history->y < 0 (which is allowed) set current to 0.
	// This means I've got to check current's defined all through this
	// function.
	struct line *current = &history->d[history->y];
	int tmp;
	set_ch (win);
	switch (chget.ch) {
		case KEY_RESIZE:
			resize ();
			break;
		case $ctrl ('l'):
			clear_all ();
			refresh_all ();
			break;
		case 'o':
			arr_insert (history, history->y + 1, (struct line) {0});
			history->y++;
			history->x = 0;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case 'O':
			arr_insert (history, history->y, (struct line) {0});
			history->x = 0;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case 's':
			arr_splice (current, history->x, 1);
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case 'S':
			current->n = history->x = 0;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case 'I': {
			// FIXME: this tmp = isn't necessary now that cmdline_caret returns
			// the current pos on failure.
			if (-1 != (tmp = cmdline_caret ()))
				history->x = tmp;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			print_insert ();
		}
			break;
		case 'A':
			if (!current->n)
				break;
			history->x = current->n - 1;
			while (isblank (current->d[history->x]) && history->x > 0)
				history->x--;
			history->x++;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			print_insert ();
			break;
		case 'i':
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			print_insert ();
			break;
		case 'r':
			ui.mode = M_CMDLINE_WINDOW_REPLACE_ONE;
			break;
		case 'R':
			ui.mode = M_CMDLINE_WINDOW_REPLACE;
			break;
		case 'a':
			if (history->x < current->n)
				history->x++;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			print_insert ();
			break;
		case 'h':
			if (current && history->x > 0)
				history->x--;
			break;
		case $ctrl ('e'):
			/* cmdline_scroll_down (); */
			if (window->pad_start.y < 1000 - history->n)
				window->pad_start.y++;
			if (y_to_pad_line (history->y, 1000) < window->pad_start.y) {
				history->y = pad_line_to_y (window->pad_start.y, 1000);
			}
			break;
		case $ctrl ('y'):
			/* cmdline_scroll_up (); */
			// FIXME: hard-coded pad size
			if (window->pad_start.y > 0) window->pad_start.y--;
			break;
		case 'j':
			if (history->y < (int) history->n - 1)
				history->y++;
			keep_x_within_range ();
			break;
		case 'k':
			// NOTE (trickiness): your y cursor is allowed to go negative.
			// Check draw_cmdline_window for a partial explanation.
			if (history->y > 0) history->y--;
			keep_x_within_range ();
			break;
		case 'l':
			if (current && history->x < current->n)
				history->x++;
			break;
		case 'b':
			history->x = mvnt_b (history->x);
			break;
		case 'B':
			history->x = mvnt_B (history->x);
			break;
		case 'w':
			history->x = mvnt_w (history->x);
			break;
		case 'W':
			history->x = mvnt_W (history->x);
			break;
		case 'e':
			history->x = mvnt_e (history->x);
			break;
		case 'E':
			history->x = mvnt_E (history->x);
			break;
		case 'x':
			if (!history->x)
				break;
			arr_splice (current, history->x, 1);
			break;
		case 'X':
			if (!current || !history->x)
				break;
			history->x--;
			arr_splice (current, history->x, 1);
			break;
		case '$':
			if (current)
				history->x = current->n;
			break;
		case '0':
			if (current)
				history->x = 0;
			break;
		case '_':
		case '^':
			if (current)
				cmdline_caret ();
			break;
		case 'D':
			current->n = history->x;
			if (history->x) history->x--;
			break;
		case 'g':
			set_ch (win);
			switch (chget.ch) {
				case 'e':
					history->x = mvnt_ge (history->x);
					break;
				case 'E':
					history->x = mvnt_gE (history->x);
					break;
				default:
					break;
			}
			break;
		case 'C':
			current->n = history->x;
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case 'd':
			if (current)
				cmdline_d ();
			break;
		case 'c':
			if (!current)
				break;
			cmdline_d ();
			ui.mode = M_CMDLINE_WINDOW_INSERT;
			break;
		case ';':
			if (!current)
				break;

			// IMPROVEMENT: this and case ',' are obviously less than
			// pretty. FIXME: search_char_in_line_bw is returning the
			// history->x on failure now, so I don't need to bother
			// with tmp.

			if (cl->last_line_search_dir > 0
					&& (tmp = search_char_in_line_fw (cl->last_line_search))) {
				history->x = tmp;
				if (cl->was_to)
					history->x--;
			} else if (cl->last_line_search_dir < 0
					&& (tmp = search_char_in_line_bw (cl->last_line_search))) {
				history->x = tmp;
				if (cl->was_to)
					history->x++;
			}
			break;
		case ',':
			if (!current)
				break;

			// FIXME: search_char_in_line_bw is returning the
			// history->x on failure now, so I don't need to bother
			// with tmp.
			if (cl->last_line_search_dir < 0
					&& (tmp = search_char_in_line_fw (cl->last_line_search))) {
				history->x = tmp;
				if (cl->was_to)
					history->x++;
			} else if (cl->last_line_search_dir > 0
					&& (tmp = search_char_in_line_bw (cl->last_line_search))) {
				history->x = tmp;
				if (cl->was_to)
					history->x--;
			}
			break;
		case 'f':
			if (!current)
				break;
			tmp = cmdline_f ();
			if (tmp)
				history->x = tmp;
			break;
		case 'F':
			if (!current)
				break;
			tmp = cmdline_F ();
			if (tmp)
				history->x = tmp;
			break;
		case 't': {
			if (!current)
				break;
			int x = cmdline_f ();
			if (x != history->x)
				history->x = x - 1;
		}
			break;
		case 'T': {
			if (!current)
				break;
			cmdline_F ();
			int x = cmdline_f ();
			if (x != history->x)
				history->x = x + 1;
		}
			break;
		case $ctrl ('m'):
		case $ctrl ('j'):
		case KEY_ENTER:
			cmdline_return (history, 1);
			break;
		case $ctrl ('c'):
			ui.mode = M_PROMPT;
			break;
		default:
			break;
	}
}

static bool is_prompt (enum mode mode) {
	return mode == M_PROMPT || ui.mode == M_PFIELD_INSERT
		|| ui.mode == M_INSTR_INSERT;
}

// For the prompt and the cmdline window. And also the p-fields.
void input_text_insert (void) {
	auto win = ui.cmdline.win;
	auto history = ui.cmdline.cl.current_history;
	auto current = &history->d[history->y];
	auto cmdline_window = &ui.cmdline_window;
	set_ch (win);
	bool did_history_search = 0;
	switch (chget.ch) {
		case KEY_RESIZE:
			resize ();
			break;

		// RULE: leave this here, even though it's repeated in all the
		// input_funcs. You will be tempted again to pass ch and write
		// this only once in an input function. But then you'll need
		// an if, because wgetch works on a specific window. You might
		// next think you'll use a function pointer. But the modes and
		// imput functions don't line up -- insert services replace
		// and replace one.
		case $ctrl ('l'):
			clear_all ();
			refresh_all ();
			break;
		case $ctrl ('f'):
			if (is_prompt (ui.mode)) {
				ui.mode = M_CMDLINE_WINDOW_NORMAL;
				cmdline_window->pad_start = (struct v) {
					.y = history->n - 7 < 0? 0: history->n - 7,
				};
			}
			break;
		case $ctrl ('p'):
			ctrl_p ();
			did_history_search = 1;
			break;
		case $ctrl ('n'):
			ctrl_n ();
			did_history_search = 1;
			break;
		case KEY_LEFT:
			if (history->x > 0)
				history->x--;
			break;
		case KEY_RIGHT:
			if (history->x < current->n)
				history->x++;
			break;
		case $ctrl ('h'):
			if (history->x > 0) {
				history->x--;
				arr_splice (current, history->x, 1);
			}
			break;
		case $ctrl ('u'):
			arr_splice (current, 0, history->x);
			history->x = 0;
			break;
		case 27:
		case $ctrl ('@'):
		case $ctrl ('c'):
			if (is_prompt (ui.mode)) {
				ui.mode = M_NORMAL;
				cmdline_return (history, 0);
			} else
				ui.mode = M_CMDLINE_WINDOW_NORMAL;
			break;
		case $ctrl ('m'):
		case $ctrl ('j'):
		case KEY_ENTER:
			cmdline_return (history, 1);
			break;
		default:
			switch (ui.mode) {
				case M_CMDLINE_WINDOW_INSERT:
				case M_PROMPT:
				case M_INSTR_INSERT:
				case M_AMP_INSERT:
				case M_PFIELD_INSERT:
					arr_insert (current, history->x++, chget.ch);
					break;
				case M_CMDLINE_WINDOW_REPLACE:
					if (history->x >= current->n)
						arr_insert (current, history->x++, chget.ch);
					else
						current->d[history->x++] = chget.ch;
					break;
				case M_CMDLINE_WINDOW_REPLACE_ONE:
					current->d[history->x] = chget.ch;
					ui.mode = M_CMDLINE_WINDOW_NORMAL;
					break;
				default:
					assert (0);
					break;
			}
			break;
	}
	if (!did_history_search)
		history->history_search_x = -1;
	if (ui.mode == M_CMDLINE_WINDOW_REPLACE_ONE)
		ui.mode = M_CMDLINE_WINDOW_NORMAL;
}

static struct event *normal_e (void) {
	auto sco = &scos.d[scos.i];
	auto channel = &sco->channels.d[sco->curs.x];
	struct event *r = 0;
	auto event = get_next_event ($sco->curs.y);
	if (!event)
		return 0;

	$flt start_row = event->row;

	while ((event = get_event_at_row (channel, start_row))) {
		start_row += 1;
		r = event;
	}
	return r;
}

static struct event *normal_b (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	struct event *r = 0;
	struct event *event = get_prev_event ($sco->curs.y);
	if (!event)
		return 0;

	$flt start_row = event->row;

	while ((event = get_event_at_row (channel, start_row))) {
		start_row--;
		r = event;
	}
	return r;
}

// FIXME: this function can go away now cursors are only dealine with
// integer rows.
static int y_from_event (struct event *event) {
	return event->row;
}


static struct event *normal_w (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	struct event *r = 0;

	// Get the event at where the cursor is
	struct event *event = get_event_at_row (channel, sco->curs.y);
	// If there's not one there,
	if (!event) {
		// Get the next event and we're done
		event = get_next_event (sco->curs.y);
		if (!event) // Don't move
			return 0;
		return event;
	}

	// If the cursor wasn't on an event, get to the end of the block
	// ...
	$flt row = event->row;
	if (event) {
		while ((event = get_event_at_row (channel, row))) {
			row++;
			r = event;
		}
	}

	// Then get to the next event
	r = get_next_event (row);
	if (!r)

		// A slight deviation from the Holy Bible -- I mean, Vim. Vim
		// will go to the next line if you w and there's no next word.
		// Here well'll go to the end of the word.
		r = &channel->d[channel->n - 1];

	return r;
}

static void update_y_from_event (int *y, struct event *event) {
	if (!event)
		return;
	*y = y_from_event (event);
}

static struct event *normal_ge (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	struct event *r = 0;

	int y = sco->curs.y;
	// Get the event at where the cursor is
	struct event *event = get_event_at_row (channel, y);
	// If there's not one there,
	if (!event) {
		// Get the next event and we're done
		event = get_prev_event (sco->curs.y);
		if (!event) // Don't move
			return 0;
		update_y_from_event (&y, event);
		return event;
	}

	// If the cursor wasn't on an event, get to the end of the block
	// ...
	$flt event_row = event->row;
	if (event) {
		while ((event = get_event_at_row (channel, event_row))) {
			event_row--;
			r = event;
		}
	}

	// Then get to the prev event
	r = get_prev_event (event_row);
	if (!r)

		// A Vim difference. ge in Vim will take you to the beginning
		// of the previous line, but I don't think that makes sense
		// here.
		r = arr_first (channel);

	return r;
}

static void ed_x (bool backspace_at_end) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	arr_remove (channel,
			(&(struct event) {
				.start = $c2t ($sco->curs.y),
				.row = $sco->curs.y,
			 }),
			sco_cmp_event_by_row, free_event);
	shift_events (channel, $sco->curs.y, -1);
	if (backspace_at_end && channel->n) {
		struct event *last_event = get_last_event ();
		if (last_event && sco->curs.y >= last_event->row)
			sco->curs.y--;
	}
}

static bool at_p_field_end (void) {
	int width =
		$sco->curs_p_field < PFT_P_FIELD
			? p_field_widths[$sco->curs_p_field]
			: p_field_widths[PFT_P_FIELD];

	if ($sco->p_field_offset == width - 1)
		return 1;
	return 0;
}

static void channel_remove_range (struct channel *channel,
		$flt begin, $flt end) {
	arr_each (channel, event) {
		if (event->start >= begin && event->start < end) {
			if (event->ps.n)
				free (event->ps.d);
			arr_splice (channel, event - channel->d, 1);
			event--;
		}
	}
}

static struct event clone_event (struct event *event) {
	struct event r = {0};
	r = *event;
	// This memset means you'll get a new allocation.
	memset (&r.ps, 0, sizeof r.ps);
	$fori (i, event->ps.n)
		arr_push (&r.ps, event->ps.d[i]);

	return r;
}

// Note this will append to r.
static void channel_clone_range (struct events *r, struct channel *channel,
		$flt begin, $flt end) {
	arr_each (channel, event) {
		if (event->start >= begin && event->start <= end) {
			struct event cloned_event = clone_event (event);
			arr_push (r, cloned_event);
		}
	}
}

#if 0
// NOTE: this is for pasting. You can't just copy over notes -- you have to update
// their start times It could take a range, but doesn't need to right now.
static void update_start_times_to (struct channel *channel, $flt to, $flt start,
		$flt end) {
	if (!channel->n)
		return;
	$flt diff = to - channel->d[0].start;
	// NOTE (perf): small performance gain could be had by getting the start and end
	// events first. Microscopic.
	$each (event, channel->d, channel->n) {
		if (event->start >= start && event->start

				// NOTE this + $sco->row_len. It means it's exclusive.
				// It means that if you do this on just one row, it
				// gets updated, which is what you want.

				< end + $sco->row_len)
			event->start += diff;
	}
}
#endif

// Copies the range to whichever reg you've specified.
//
// You have to have specified one somewhere. " will specify it. Yank
// by itself (y) will specify the zero register.
//
// This is a simplification of what Vim does. I'm not bothering with
// 1, 2, 3, etc. And I should say might have some of the detail wrong.
// I've got over h :registers again and I don't really get it.
//
// Vim's often lauded for its help. Its help is extensive, but not
// always unbelievably clear. I should say, though, that I've tried
// writing documention and it is quite literally the most difficult
// thing in the world.

static void set_reg (struct events *reg, bool append, $flt start, $flt end) {
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	if (!reg)
		reg = &scos.reges.zero;
	if (!append) {
		free_channel ((struct channel *) reg);
		reg->n = 0;
	}
	channel_clone_range (reg, channel, start, end);
	// Change the reg so the start start from 0.
	$flt reg_first_row = reg->d[0].row;
	shift_events ((struct channel *) reg, $sco->curs.y, -reg_first_row);
	scos.reges.unnamed = reg;
}

static struct range motion (struct events *specified_reg, bool append, char operator) {
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	struct event *mark = 0;
	struct window *editor = &ui.editor;
	set_ch (ui.editor.win);
	struct range r = {. start = $sco->curs.y, .end = -1};
	// This is for, eg, yy. We'll see how well this works.
	if (chget.ch == operator) {
		r.start = 0;
		r.end = channel->d[channel->n - 1].row;
		return r;
	}
	switch (chget.ch) {
		case 'j':
		case 'l':
			if ($sco->curs.y >= editor->pad_start.y + editor->dims.y - 2)
				straight_scroll_down (&editor->pad_start.y, &$sco->curs.y,
						&$sco->dims.y, 1);
			r.end = $sco->curs.y + 1;
			break;
		case 'k':
		case 'h':
			if ($sco->curs.y <= editor->pad_start.y)
				straight_scroll_up (&editor->pad_start.y, &$sco->curs.y, &$sco->dims.y, 1);
			r.end = $sco->curs.y - 1;
			break;
		case '$':
			if (channel->n) {
				mark = &channel->d[channel->n - 1];
				r.special_addition = 1;
				/* r.end = mark->start; */
			}
			break;
		case '0':
			r.end = 0;
			break;
		case '_':
		case '^':
			if (channel->n) {
				mark = &channel->d[0];
			}
			break;
		case 'e':
			mark = normal_e ();
			if (mark) {
				r.end = mark->row;
				r.special_addition = 1;
			}
			break;
		case 'w':
			mark = normal_w ();
			if (mark == &channel->d[channel->n - 1])
				r.special_addition = 1;
			break;
		case 'b':
			mark = normal_b ();
			break;
		case 'g':
			set_ch (ui.editor.win);
			switch (chget.ch) {
				case 'e':
					mark = normal_ge ();
					break;
			}
			break;
		default:
			break;
	}
	if (mark)
		r.end = mark->row;

	if (r.end == -1) {
		r.failed = 1;
		return r;
	}
	if (r.end < r.start) {
		$swap (r.end, r.start);
		$sco->curs.y = r.start;
		update_y_from_event (&$sco->curs.y, mark);
	}

	return r;

}

static void normal_d (struct events *specified_reg, bool append) {
	struct range range = motion (specified_reg, append, 'd');
	if (range.failed)
		return;

	// See struct range in types.h for an over-the-top explanation of
	// this.

	if (range.special_addition)
		range.end += range.special_addition;
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	set_reg (specified_reg, append, range.start, range.end);
	channel_remove_range (channel, range.start, range.end);

	// FIXME: put in, eg, drw to keep the notes from pulling up.

	shift_events (channel, $sco->curs.y, -(range.end - range.start));
}

static void chmod_insert (void) {
	ui.mode = M_INSERT;
	print_insert ();
}

static void chmod_replace (void) {
	ui.mode = M_REPLACE;
	print_replace ();
}

static void normal_right_brace (void) {
	if ($sco->curs.x < $sco->channels.n - 1) {
		$sco->curs.x++;
		$sco->curs_p_field = 0;
		$sco->p_field_offset = 0;
	}
}

static inline int get_p_field_offset (int curs_p_field) {
	return curs_p_field < 3
				? p_field_widths[curs_p_field] - 1
				: p_field_widths[PFT_P_FIELD] - 1;
}

static void normal_left_brace (void) {
	if ($sco->curs.x > 0) {
		$sco->curs.x--;
		struct channel *channel = &$sco->channels.d[$sco->curs.x];
		$sco->curs_p_field = 3 + channel->most_ps;
		$sco->p_field_offset = get_p_field_offset ($sco->curs_p_field);
	} else
		// This means { will take you to the left side of the first channel
		// if you're already in the first channel. This is what Vim does.
		// It's also, I'll have you know, an obvious thing to do.
		$sco->curs_p_field = $sco->p_field_offset = 0;
}

static void normal_gl (void) {
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	if ($sco->curs_p_field < 3 + channel->most_ps) {
		$sco->curs_p_field++;
		$sco->p_field_offset = 0;
	} else
		normal_right_brace ();
}

static void normal_gh (void) {
	if ($sco->curs_p_field > 0) {
		$sco->curs_p_field--;
		$sco->p_field_offset = get_p_field_offset ($sco->curs_p_field);
	} else
		normal_left_brace ();
}

struct event *normal_f (void) {
	struct event *r = {0};
	switch ($sco->curs.x) {
		case PFT_NOTE:
		case PFT_AMP:
		case PFT_INSTR:
		default:
			break;
	}
	return r;
}


static void normal_p (struct events *reg) {

	// If you've just done p, use the "unnamed buffer". In Vim you can
	// access that specifically with ", ie, "". I won't do that.
	//
	// The unnamed buffer is a pointer to the last thing you
	// yanked/deleted.

	if (!reg || !reg->n)
		reg = scos.reges.unnamed;

	struct channel *channel = &$sco->channels.d[$sco->curs.x];

	// reg_start should alway be 0, because yanked registers should
	// always be shifted so their starts start at 0.

	$flt reg_end = arr_last (reg)->start;

	$flt curs_row = $sco->curs.y;

	// Need to make a clone of reg because otherwise reg's starts
	// won't start from 0. I feel like I'm doing too much clone and
	// shifting. But whatever.
	//
	// Also, it'd be clearer and probably faster if I just shifted reg
	// directly and then shifted it back to 0.

	struct events tmp_reg = {0};
	$each (r, reg->d, reg->n)
		arr_push (&tmp_reg, *r);

	// Update the tmp register's start times so they slot in the right
	// place.

	shift_events ((struct channel *) &tmp_reg, 0, curs_row);

	// Shift along the channel events that the paste is going to
	// displace.

	shift_events (channel, curs_row, reg_end + 1);

	// Just push the events and qsort, rather than trying to insert it
	// in the right place.

	$fori (i, tmp_reg.n) {
		struct event cloned_event = clone_event (&tmp_reg.d[i]);
		arr_push ((struct channel *) channel, cloned_event);
	}
	// Don't free the events: they're in the channel now.
	free (tmp_reg.d);
	$qsort (channel->d, channel->n, sco_cmp_event_by_row);
}

static void yank (struct events *specified_reg, bool append) {
	struct range range = motion (specified_reg, append, 'd');
	if (range.failed)
		return;
	set_reg (specified_reg, append, range.start, range.end);
}

// Returns 0 if the ch was dealt with -- ie, you type a y, or whatever
// -- else 1.

static int operator (int ch, struct events *reg, bool append) {
	int r = 0;
	switch (ch) {
		case 'd':
			normal_d (reg, append);
			break;
		case 'y':
			yank (reg, append);
			break;
		case 'c':
			normal_d (reg, append);
			chmod_insert ();
			break;

		// In Vim, p and P aren't operators. They're not, here,
		// either, but this is here because of the way I've laid out
		// the code.
		//
		// Potentially I could rename this func "action".

		case 'p':
			$sco->curs.y++;
			normal_p (reg);
			break;
		case 'P':
			normal_p (reg);
			break;
		case 'f':
			normal_f ();
			break;
		default:
			r = 1;
			break;
	}
	return r;
}

static void normal_dquote (void) {
	set_ch (ui.editor.win);
	struct events *reg;
	struct reges *reges = &scos.reges;
	bool append = 0;
	switch (chget.ch) {
		case 'A' ... 'Z':
			append = 0;
			__attribute__((fallthrough));
		case 'a' ... 'z':
			reg = &reges->letters[chget.ch - 'a'];
			reges->unnamed = reg;
			break;
		default: return;
	}
	set_ch (ui.editor.win);
	operator (chget.ch, reg, append);
}

static int scrolling (int ch) {
	struct window *editor = &ui.editor;
	struct sco *sco = &scos.d[scos.i];
	int r = 0;
	switch (ch) {
		case $ctrl ('f'):
			straight_scroll_down (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					editor->dims.h);
			break;
		case $ctrl ('b'):
			straight_scroll_up (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					editor->dims.h);
			break;
		case $ctrl ('d'):
			straight_scroll_down (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					editor->dims.h / 2);
			break;
		case $ctrl ('e'):
			straight_scroll_down (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					1);
			break;
		case $ctrl ('y'):
			straight_scroll_up (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					1);
			break;
		case $ctrl ('u'):
			straight_scroll_up (&editor->pad_start.y, &sco->curs.y, &sco->dims.h,
					editor->dims.h / 2);
			break;
		default:
			r = 1;
	}
	return r;
}

static int normal_digit (int *ch) {
	int r = 0;
	// INT_MAX has 10 digits.
	int max_digits = 10 + 1;
	char digits[max_digits];
	int n_digits = 0;

	do {
		int num = *ch - '0';
		if (num < 0 || num > 9)
			break;
		if (n_digits > 10 - 1) {
			print_msg ("You're only allowed 10 digits");
			break;
		}
		digits[n_digits++] = num;
		set_ch (ui.editor.win);
	} while (1);

	int magnitude = 1;
	$forri (i, n_digits) {
		r += digits[i] * magnitude;
		magnitude *= 10;
	}

	return r;
}

static void end_macro (void) {
	chget.macro.n = chget.macro.i = 0;
	chget.state = GCF_GET;
}

// Assumes you're already in GCF_RECORD.
static void repeat_normal (int ch, int repeats) {
	arr_push (&chget.macro, ch);
	// This chget.swallow is explained in input_normal
	chget.swallow = 1;
	input_normal ();
	chget.state = GCF_PLAY;

	// I've not done anything to keep macros from spinning. Like if
	// you were to repeat an action 500 times, it'd do it 500 times.
	// It won't run through the entire code path. Example: if a motion
	// fails, normal_d will return. Vim doesn't do anthing fancier.
	// Actually, it probably does. 50000 de doesn't make the editor
	// spin.

	$fori (i, repeats - 1)
		input_normal ();
	end_macro ();
}

// Assumes you're already in GCF_RECORD.
//
// FIXME: I was right the first time. This is not a priority, but
// input_insert should basically be an operator that ends when you're
// done with being in insert mode. I was worried about infinite
// recursion, but that's nonsense. When you leave insert mode you're
// back in normal mode, ie you've popped the stack. If I do make
// insert mode calls inside input_normal, I can get rid of this
// function and the swallow char thing.

static void repeat_insert (int repeats) {

	enum mode mode = ui.mode;
	// Unlike repeat_normal, we don't swallow the ch.
	input_insert ();
	chget.state = GCF_PLAY;
	$fori (i, repeats - 1) {
		ui.mode = mode;
		input_insert ();
	}
	chget.ch = 0;
	end_macro ();
}

static int insert_enterers (void) {
	int r = 0;
	switch (chget.ch) {
		case 'i':
			chmod_insert ();
			break;
		case 'r':
			ui.mode = M_REPLACE_ONE;
			break;
		case 'I':
			update_y_from_event (&$sco->curs.y, normal_caret ());
			chmod_insert ();
			break;
		case 'a':
			$sco->curs.y += 1;
			chmod_insert ();
			break;
		case 'R':
			chmod_replace ();
			break;
		case 's':
			ed_x (0);
			chmod_insert ();
			break;
		default:
			r = 1;
			break;
	}
	return r;
}

// Used for <c-x> and <c-a>. Increment, decrement note by half step.
// You could pass it 4, -2, whatever, but I'll just be passing it 1 or
// -1, and using repeat to do it more.

static void normal_c_x_a (int half_steps) {
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	struct event *event = get_event_at_row (channel, $sco->curs.y);
	if (!event)
		return;
	// CAREFUL: This seems to work, but it's not tested.
	$flt current_steps = fc_steps_from_frequency (event->cps, $c4, 1);
	current_steps += half_steps;
	event->cps = fc_frequency_by_step (current_steps, $c4);
}

void input_normal (void) {

	WINDOW *win = ui.editor.win;
	struct ed *ed = &ui.editor.ed;
	struct window *editor = &ui.editor;
	struct sco *sco = &scos.d[scos.i];
	struct orc *orc = orcs.n? &orcs.d[orcs.i]: 0;
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	struct event *event = get_event_at_row (channel, sco->curs.y);

	// FIXME: I don't think this if (!ch) is/should be necessary.
	// Something like it is. Look: 5de will delete to the end 5 times.
	// You type 5, and I gather a number, and I know you're done
	// typing numbers because you've typed a d. So, you've already got
	// your char. But this isn't the case for repeating insert mode
	// cmds. So I can't make SWALLOW part of the get_ch state.

	set_ch (win);
	if (0 == operator (chget.ch, 0, 0))
		return;
	if (0 == scrolling (chget.ch))
		return;
	if (0 == insert_enterers ())
		return;

	switch (chget.ch) {
		case KEY_RESIZE:
			resize ();
			break;
		case $ctrl ('m'):
		case $ctrl ('j'):
		case KEY_ENTER:
		case KEY_F (5):
			if (0 == start_play (0, 0))
				ui.editor.ed.flags |= EF_COMPILED;
			break;
		case '"':
			normal_dquote ();
			break;
		case $ctrl ('x'):
			normal_c_x_a (-1);
			break;
		case $ctrl ('a'):
			normal_c_x_a (1);
			break;
		case '1' ... '9': {
			int repeats = normal_digit (&chget.ch);
			chget.state = GCF_RECORD;
			// FIXME: Make this a function/data.
			if (chget.ch == 'i' || chget.ch == 'r' || chget.ch == 'R'
					|| chget.ch == 'a' || chget.ch == 'A' || chget.ch == 's'
					|| chget.ch == 'S' || chget.ch == 'I') {
				insert_enterers ();
				ui.changed = 1;
				repeat_insert (repeats);
			} else
				repeat_normal (chget.ch, repeats);
		}
			break;
		case $ctrl ('n'):
			if (orc)
				orc->instr++;
			break;
		case $ctrl ('p'):

			// Can't have an instr of 0. Probably be an option later
			// to let you.

			if (orc && orc->instr > 1)
				orc->instr--;
			break;
		case '^':
		case '_':
			update_y_from_event (&sco->curs.y, normal_caret ());
			break;
		case 'e':
		case 'E':
			update_y_from_event (&sco->curs.y, normal_e ());
			break;
		case 'b':
		case 'B':
			update_y_from_event (&sco->curs.y, normal_b ());
			break;
		case 'w':
		case 'W':
			update_y_from_event (&sco->curs.y, normal_w ());
			break;
		case 'L':
			// TODO: potentially L, H and M go horizontally.
			break;
		case 'H':
			break;
		case 'M':
			break;
		case 'o':
			arr_insert (&sco->channels, sco->curs.x + 1,
					(struct channel) {0});
			sco->curs.x++;
			// I'm sending the cursor to the top of the screen
			// because that's mostly what Vim does. It might
			// get annoying, but I'll start with what Vim does.
			sco->curs.y = 0;
			break;
		case 'O':
			arr_insert (&sco->channels, sco->curs.x,
					(struct channel) {0});
			// I'm sending the cursor to the top of the screen
			// because that's mostly what Vim does. It might
			// get annoying, but I'll start with what Vim does.
			sco->curs.y = 0;
			break;
		case 'g':
			set_ch (win);
			switch (chget.ch) {
				// Like Vim's ga. Just prints the float val. In doing so it's
				// informed that my notes are all off. A4 is 261. It might be the
				// names that are off.
				case 'a':
					if (event) {
						switch ($sco->curs_p_field) {
							case 0:
								print_msg ("%f\n", event->cps);
								break;
							case 1:
								print_msg ("%d\n", event->instr);
								break;
							case 2:
								print_msg ("%f\n", event->amp);
								break;
							default:
								if (!event->ps.n)
									break;
								print_msg ("%f\n",
										event->ps.d[$sco->curs_p_field - 3].val);
								break;
						}
					}
					break;
					// Vim distinguishes between me and M. Here,
					// I could have m go half way down the buffer?
				//
				// NOTE: gm, g^ and g$, do, here, roughly what they do
				// in Vim -- go half way through the screen, to the
				// top of the screen, and to the end of the screen.
				// Except it's flipped horizontally, as most things
				// are in this program.
				//
				// However, it might be better to make these M, H and
				// L, since you'll use them pretty often. Because
				// "lines" will be much longer than your lines in a
				// text editor.
				//
				// I can see myself, in the end, just punting the
				// decision to the user. Writing, in the README feel
				// free to rebind these keys.

				case 'M':
				case 'm':
					sco->curs.y = (editor->pad_start.y + editor->dims.y - 1) / 2;
					break;
				case '^':
					sco->curs.y = editor->pad_start.y;
					break;
				case '$':
					// More off-by-one? Why is this - 2?
					sco->curs.y = editor->pad_start.y + editor->dims.y - 2;
					break;
				case ':':
					ui.mode =
							sco->curs_p_field == PFT_INSTR
								? M_INSTR_INSERT
							: sco->curs_p_field == PFT_AMP
								? M_AMP_INSERT
							: sco->curs_p_field != PFT_NOTE
								? M_PFIELD_INSERT
								: ui.mode;

					break;
				// FIXME: a copy-paste from ( and )
				case 'b':
				case 'h':
					normal_gh ();
					break;
				case 'w':
				case 'l':
					normal_gl ();
					break;
				/* case '$': */
					/* if (event) */
					/* 	sco->curs_p_field = event->ps.n? 3 + event->ps.n - 1: 3; */
					/* else */
					/* 	sco->curs_p_field = 3; */
					/* break; */
				case '0':
					sco->curs_p_field = 0;
					break;
				/* case '^': */
				/* case '_': */
				/* 	// FIXME: this should go to the first non-space */
				/* 	sco->curs_p_field = 0; */
					break;
				case 'o':
					if (ed->octave < $max_octave)
						ed->octave++;
					break;
				case 'i':
					if (ed->octave > $min_octave)
						ed->octave--;
					break;
				case $ctrl ('x'):
					normal_c_x_a (-12);
					break;
				case $ctrl ('a'):
					normal_c_x_a (12);
					break;
				case 'e':
				case 'E':
					update_y_from_event (&sco->curs.y, normal_ge ());
					break;
				default: goto out;
			}
			break;
		case '0':
			editor->pad_start.y = 0;
			sco->curs.y = 0;
			break;
		case '$':
			break;
		case $ctrl ('l'):
			clear_all ();
			refresh_all ();
			break;
		case 27:
		case $ctrl ('@'):
		case $ctrl ('c'):
			if (ed->flags & EF_PLAYING) {
				ed->flags &= ~EF_PLAYING;
				wclear (win);
				wclear (ui.cmdline.win);
			}
			break;
		case ':':
			ui.mode = M_PROMPT;
			break;
		case '{':
			normal_left_brace ();
			break;
		case '}':
			normal_right_brace ();
			break;
		case 'h':
			if (sco->p_field_offset == 0) {
				normal_gh ();
			} else
				sco->p_field_offset--;

			break;
		case 'l':
			if (at_p_field_end ()) {
				normal_gl ();
			} else
				sco->p_field_offset++;
			break;
		case '(':
			normal_gh ();
			break;
		case ')':
			normal_gl ();
			break;
		case 'j':
			// DODGY: As always, me with my off-by-ones. Why this - 2?
			// Why not - 1? I don't know. Basic maths, no doubt.
			if (sco->curs.y >= editor->pad_start.y + editor->dims.y - 2)
				straight_scroll_down (&editor->pad_start.y, &sco->curs.y, &sco->dims.y, 1);
			sco->curs.y += 1;

			break;
		case 'k':
			if (sco->curs.y <= editor->pad_start.y)
				straight_scroll_up (&editor->pad_start.y, &sco->curs.y, &sco->dims.y, 1);
			sco->curs.y -= 1;
			break;
		case 'X':
			if (sco->curs.y > 0)
				sco->curs.y--;
			// FIXME: this keep removing notes when you're at the start.
			ed_x (0);
			break;
		case 'x':
			ed_x (1);
			break;
	}
out:
	return;
}

static void set_note_inner (struct event *event, enum fc_note_name note,
		int octave_offset) {
	struct ed *ed = &ui.editor.ed;
	event->cps = fc_frequency_by_note (note, ed->octave + octave_offset, $c4);
}

static int set_note (struct event *event, int ch) {
	// FIXME: instr shouldn't be in orc. It should be in sco.
	// This will crash if you don't have an orc loaded.
	switch (ch) {
		case 'z': set_note_inner (event, FCN_C, 0); 		break;
		case 's': set_note_inner (event, FCN_C_SHARP, 0);	break;
		case 'x': set_note_inner (event, FCN_D, 0); 		break;
		case 'd': set_note_inner (event, FCN_D_SHARP, 0);	break;
		case 'c': set_note_inner (event, FCN_E, 0); 		break;
		case 'v': set_note_inner (event, FCN_F, 0); 		break;
		case 'g': set_note_inner (event, FCN_F_SHARP, 0);	break;
		case 'b': set_note_inner (event, FCN_G, 0); 		break;
		case 'h': set_note_inner (event, FCN_G_SHARP, 0);	break;
		case 'n': set_note_inner (event, FCN_A, 0); 		break;
		case 'j': set_note_inner (event, FCN_A_SHARP, 0);	break;
		case 'm': set_note_inner (event, FCN_B, 0); 		break;
		case ',': set_note_inner (event, FCN_C, 1); 		break;
		case 'l': set_note_inner (event, FCN_C_SHARP, 1);	break;
		case '.': set_note_inner (event, FCN_D, 1); 		break;
		case 'q':
				  set_note_inner (event, FCN_C, 1);
				  break;
		case '2': set_note_inner (event, FCN_C_SHARP, 1);	break;
		case 'w': set_note_inner (event, FCN_D, 1); 		break;
		case '3': set_note_inner (event, FCN_D_SHARP, 1);	break;
		case 'e': set_note_inner (event, FCN_E, 1); 		break;
		case 'r': set_note_inner (event, FCN_F, 1); 		break;
		case '5': set_note_inner (event, FCN_F_SHARP, 1);	break;
		case 't': set_note_inner (event, FCN_G, 1); 		break;
		case '6': set_note_inner (event, FCN_G_SHARP, 1);	break;
		case 'y': set_note_inner (event, FCN_A, 1); 		break;
		case '7': set_note_inner (event, FCN_A_SHARP, 1);	break;
		case 'u': set_note_inner (event, FCN_B, 1); 		break;
		case 'i': set_note_inner (event, FCN_C, 2); 		break;
		case '9': set_note_inner (event, FCN_C_SHARP, 2);	break;
		case 'o': set_note_inner (event, FCN_D, 2); 		break;
		case 'p': set_note_inner (event, FCN_D_SHARP, 2);	break;
		case '-': set_note_inner (event, FCN_E, 2); 		break;
		case '[': set_note_inner (event, FCN_F, 2); 		break;
		default:
				  return 1;
	}
	return 0;
}

static int change_hex_digit (int *r, int ch, int preamble,
		char *p_field_name, int range[0]) {
	int code = 1;
	static char str[8];
	// FIXME: this will fail miserable when I allow values
	// over 0xff.
	sprintf (str, "%02x", *r);
	int p_field_offset = $sco->p_field_offset;
	int _res = 0;
	switch (ch) {
		case '0' ... '9':
		case 'a' ... 'f':
		case 'A' ... 'F':
			str[p_field_offset - preamble] = ch;
			_res = strtol (str, 0, 16);
			code = 0;
			break;
		default:
			print_msg ("Digits must be between 0 and F hex");
			break;
	}
	if (_res < range[0] || _res > range[1]) {
		print_msg ("%s must be between %d and %d", p_field_name,
				range[0], range[1]);
	} else
		*r = _res;
	return code;
}

static int change_hex_digit_from_float ($flt *r,
		int ch, int preamble, char *p_field_name, int range[2]) {
	int hex = flt_to_hex (*r);
	int code = change_hex_digit (&hex, ch, preamble,
			p_field_name, range);
	if (code)
		return code;
	*r = hex_to_flt (hex);
	return code;
}

static int change_p_field (struct p_field *r, int ch, int preamble) {
	int p_field_offset = $sco->p_field_offset;
	if (p_field_offset <= 1) {
		int id = strtol ((char []) {ch, 0}, 0, 16);
		if (errno) {
			print_msg ("%c isn't a digit", ch);
			errno = 0;
			return 1;
		}
		if (change_hex_digit (&id, ch, 0, "p-fields",
				(int []) {0x6, 0xff}))
			return 1;

		r->id = id;
		return 0;
	}

	if (r->id == -1 && p_field_offset > 1) {
		print_msg ("You must put the p-field number in first");
		return 1;
	}

	int code = change_hex_digit_from_float (&r->val, ch, preamble,
			"p-fields", (int []) {0x0, 0xff});

	return code;
}

static void set_event (void) {
	struct sco *sco = &scos.d[scos.i];
	struct channel *channel = &sco->channels.d[sco->curs.x];
	struct event *event = get_event_at_row (channel, sco->curs.y);
	struct orc *orc = &orcs.d[orcs.i];
	bool was_new_event = 0;
	// I've learned something about scope. Well, I should have guessed.
	//
	// I can't do
	// 		whatever *p;
	// 		{
	// 			whatever thing;
	// 			p = &thing;
	// 		}
	//
	// 	I seem to get away with it. But libasan didn't like it at all.
	// 	A bit sad that I can't do it, because it would be neater.
	struct event possible_new_event  = sco_make_event (orc->instr, sco->curs.x,
				sco->curs.y, $c2t (sco->curs.y));
	if (ui.mode == M_INSERT || !event) {
		was_new_event = 1;
		event = &possible_new_event;
	}
	int ret = 0;
	switch ($sco->curs_p_field) {
		case PFT_NOTE:
			ret = set_note (event, chget.ch);
			break;
		case PFT_INSTR:
			ret = change_hex_digit (&event->instr, chget.ch, 0,
					"instr", (int []) {0x1, 0xff});
			break;
		case PFT_AMP:
			ret = change_hex_digit_from_float (&event->amp, chget.ch, 0,
					"amp", (int []) {0x0, 0xff});
			break;
		default: { // p-fields
			int idx = $sco->curs_p_field - 3; // The three default p-fields
			if (event->ps.n <= idx) {
				struct p_field new_p_field = {.id = -1};
				if (0 == (ret = change_p_field (&new_p_field, chget.ch, 2)))
					arr_push (&event->ps, new_p_field);
			} else
				ret = change_p_field (&event->ps.d[idx], chget.ch, 2);
		}
			break;
	}
	if (0 == ret) {
		if (was_new_event) {
			/* Note: This is a pretty expensive way to do this. I have an */
			/* arr_add_by_member, or similar in darr.h. */
			if (ui.mode == M_INSERT)
				shift_events (channel, $sco->curs.y, 1.0);
			else // You could do if (channel->n), but it's not empty enough.
				arr_remove (channel, &event, sco_cmp_event_by_row, free_event);
			arr_push (channel, *event);
			$qsort (channel->d, channel->n, sco_cmp_event_by_row);
		}

		// This'll qsort when you're placing your first not, but it
		// doesn't hurt.

		// Don't increment cursor if you've given invalid input.

		if (ui.mode != M_REPLACE_ONE)
			sco->curs.y++;
	}
}

void input_insert (void) {
	auto win = ui.editor.win;
	auto sco = &scos.d[scos.i];
	auto channel = &sco->channels.d[sco->curs.x];

	while (1) {
		struct event *event = 0;
		set_ch (win);
		switch (chget.ch) {
			case KEY_RESIZE:
				resize ();
				break;
			case $ctrl ('l'):
				clear_all ();
				refresh_all ();
				break;
			case 27:
			case $ctrl ('c'):
			case $ctrl ('@'):
				ui.mode = M_NORMAL;
				print_msg ("");
				goto out;
			case $ctrl ('i'): {
				int next = next_multiple_of ($sco->curs.y, 8);
				shift_events (channel, $sco->curs.y, next - sco->curs.y);
				sco->curs.y = next;
			}
				break;
			case $ctrl ('u'):
				event = get_prev_event (sco->curs.y);
				if (event) {
					$each (e, channel->d, (event - channel->d))
						free_event (e);

					// FIXME (one day): there should be a version of
					// arr_splice that takes a free function.
					//
					// WATCH OUT (dodgyness): how come I have to do 1
					// + here? Well, it's 'cause I want to include the
					// current event. But. But in the words of At the
					// Drive In, "Beware."

					arr_splice (channel, 0, 1 + (event - channel->d));
				}
				sco->curs.y = 0;
				break;
			case $ctrl ('h'):
				event = get_prev_event (sco->curs.y);
				if (event) {
					free_event (event);
					arr_splice (channel, event - channel->d, 1);
					if (sco->curs.y > 0)
						sco->curs.y--;
				}
				shift_events (channel, $sco->curs.y, -1);
				break;
			case ' ':
				if (ui.mode != M_REPLACE && ui.mode != M_REPLACE_ONE)
					shift_events (channel, $sco->curs.y, 1);
				else
					arr_remove (channel,
							(&(struct event) {
								.start = $c2t ($sco->curs.y),
								.row = sco->curs.y,
							}),
							sco_cmp_event_by_row, free_event);
				if (ui.mode != M_REPLACE_ONE)
					sco->curs.y += 1;
				break;
			case '.':

				// This isn't Vim-like. Maybe it won't last. But the
				// point is this. Normally in trackers it's like
				// you're always typing in replace mode. You won't
				// push notes down. But in this -- for now, at least
				// -- I'm having i be exactly like Vim.
				//
				// So I'm expecting you to use replace mode a lot more
				// than you do in Vim. I'm also expecting the
				// hypothetical users of this software to just say
				// "Fuck it; this is stupid" and just swap i and R.
				// I'm not going to, because I'm really interested if
				// pulling up notes and inserting them changes the way
				// I write melodies. Also, since this is a proper _Vi_
				// style editor, you could be able to add/remove at
				// the speed of thought. And even if you make a
				// mistake, eventually (when I figure out how to do
				// it) there will be undo.
				//
				// Anyway, point is: press . in replace mode to
				// increment the cursor without replacing the note.
				if (ui.mode == M_REPLACE)
					sco->curs.y++;
				break;
			default:
				set_event ();
				break;
		}
		ui.changed = 1;

		if (ui.mode == M_REPLACE_ONE) {
			if (chget.state != GCF_GET)
				$sco->curs.y++;
			ui.mode = M_NORMAL;
			break;
		}
	}
out:
	return;
}
