#include "misc.h"

int str_compar (const void *a, const void *b) {
	return strcmp (*(const void **) a, *(const void **) b);
}

// Calling this *_r is a bit redundant, since there's no other way you
// could do a strn_compar.
int strn_compar_r (const void *a, const void *b, void *user) {
	int a_len = *(int *) user;
	return strncmp (*(const void **) a, *(const void **) b, a_len);
}

bool is_all_space (size_t n, char *str) {
	if (!str && !n)
		return 1;
	$each (p, str, n) {

		// This should ignore zeroes in the string

		if (*p && !isspace (*p))
			return 0;
	}
	return 1;
}

// Similar to skipnblank bit since this takes p and returns it, a p
// can be initialised with it.

char *get_skipnblanked (size_t n, char *p) {
	while (p < (p + n) && isblank (*p))
		p++;
	return p;
}

void skipnblank (char **_p, int n) {
	char *p = *_p;
	while (p < (*_p + n) && isblank (*p))
		p++;
	*_p = p;
}

int flt_to_hex ($flt flt){
	return flt * ($flt) 0xff;
}

$flt hex_to_flt (int hex) {
	return 1.0 / 0xff * hex;
}

int next_multiple_of (int x, int of) {

	// NOTE: I got this from
	// https://stackoverflow.com/questions/2403631/how-do-i-find-the-next-multiple-of-10-of-any-integer
	//
	// To stay on eg, 8 if x is already eight, do "if (x % 10)" first.
	//
	// Further down in stackoverflow there's an apparently better
	// version. This will do.

	return x + (of - x % of);
}

int strip (char *s, int n_s) {
	char *p = s;

	while (isspace (p[n_s - 1])) p[--n_s] = 0;
	while (*p && isspace (*p)) ++p, --n_s;

	memmove (s, p, n_s + 1);
	return n_s;
}

int n_char_xs_in_str (char *s, char _the_char) {
	int r = 0;
	$each_sent (p, s) {
		if (*p == _the_char) {
			r++;
		}
	}
	return r;
}


