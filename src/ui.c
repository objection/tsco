#define _GNU_SOURCE
#include "ui.h"
#include "sco.h"
#include "misc.h"

struct ui ui;

void print_msg_ap (char *fmt, va_list ap) {
	char buf[PATH_MAX];
	vsnprintf (buf, PATH_MAX, fmt, ap);
	if (!ui_initialised) {
		printf ("%s\n", buf);
		return;
	}
	// FIXME: this isn't enough. The msg_height will also need to
	// consider whether the line is longer than COLS. It'll that's
	// easy enough. I'd just insert line breaks at COLS (or COLS - 1).
	// You'd have to redo it when the resized even happens.
	//
	// Note: I'm just incrementing the size of the cmdline_window.
	// Perhaps this is too obscure a thing to do. The size of the
	// window indicates whether there's a hit enter prompt.
	ui.cmdline_window.dims.y += n_chars_xs_in_str (buf, '\n') + 1;
	struct cl *cl = &ui.cmdline.cl;
	wclear (ui.cmdline.win);
	mvwprintw (ui.cmdline.win, ui.cmdline_window.dims.y - msg_height, 0, "%s", buf);

	// TODO: do something with these saved errors.
	arr_push (&cl->errors, buf);
	refresh_cmdline ();
	doupdate ();
}

void print_msg (char *fmt, ...) {
	va_list ap;
	va_start (ap, fmt);
	print_msg_ap (fmt, ap);
	va_end (ap);
}

void clear_all (void) {
	wclear (ui.editor.win);
	wclear (ui.cmdline.win);
	wclear (ui.top_border.win);
}

static int print_cps (char *buf, double val) {
	return snprintf (buf, 32, "%.1f", val);
}

static int print_symbolic (char *buf, double val) {
	return snprintf (buf, 32, "%s%d",
				fc_note_by_frequency (val, $c4),
				fc_octave_by_frequency (val, $c4, 0));
}

// NOTE: I'm not including cmd.c, so can't use enum set_notation_sc,
// so the order of these functions might become wrong.

static int (*print_funcs[2]) (char *, double val) = {
	[0] = print_symbolic,
	[1] = print_cps,
};

static void draw_left_border (void) {

	struct sco *sco = &scos.d[scos.i];

	WINDOW *win = ui.editor.win;
	wmove (win, 0, 0);
	$fori (y, sco->dims.h)
		mvwprintw (win, y, 0, "%d", y);
}

// NOTE (minor perf): this could be pre-calculated. Rather than
// storing most_ps in struct channel, could store the calculated
// value.

static int get_cell_width (struct channel *channel) {

	// The three default p_fields
	int r = p_field_widths[PFT_NOTE] + p_field_widths [PFT_INSTR]
		+ p_field_widths[PFT_AMP];
	r += 3; // A space after each.

	// There's always space for one more p_field.
	int n_pfield_spaces = channel->most_ps + 1;

	// 3 spaces per p-field.
	r += p_field_widths[PFT_P_FIELD] * n_pfield_spaces;
	// And their spaces. No space after the last one.
	r += p_field_widths[PFT_P_FIELD] - 1;
	return r;
}

static void move_to_cell (struct v curs, int curs_p_field, int curs_p_field_offset) {
	WINDOW *win = ui.editor.win;
	struct ed *ed = &ui.editor.ed;
	struct channel *channel = &$sco->channels.d[$sco->curs.x];
	wmove (win, curs.y, ed->margin + (curs.x * get_cell_width (channel)) + curs_p_field);
}

static void move_to_p_field_and_offset (void) {
	WINDOW *win = ui.editor.win;
	struct v *curs = &$sco->curs;
	struct ed *ed = &ui.editor.ed;
	struct channel *channel = &$sco->channels.d[$sco->curs.x];

	int p_field_start = 0;

	// Ignore this hard-coded switch or just pretend I did it because
	// of efficiency.
	switch ($sco->curs_p_field) {
		case 0:
			p_field_start = 0; // "C-4 "
			break;
		case 1:
			p_field_start = p_field_widths[PFT_NOTE] + 1; // "FF "
			break;
		case 2:
			p_field_start = p_field_widths[PFT_NOTE] +
				p_field_widths[PFT_INSTR] + 1 + 1; // "FF "
			break;
		default:
			p_field_start = p_field_widths[PFT_NOTE] +
				p_field_widths[PFT_INSTR] +
				p_field_widths[PFT_AMP] + 1 + 1 + 1;
			$fori (i, channel->most_ps) {
				p_field_start += 4;
				if (i != channel->most_ps - 1)
					p_field_start += 1;
			}
			break;
	}

	int offset = p_field_start + $sco->p_field_offset;

	wmove (win, curs->y, ed->margin + (curs->x * get_cell_width (channel)) + offset);

}

static void move_to_event_cell (int channel, struct event *event,
		int curs_p_field, int curs_p_field_offset) {
	move_to_cell ((struct v) {
			.x = channel,
			.y = event->start
	}, curs_p_field, curs_p_field_offset);
}

static inline void draw_cell (int channel, struct event *event) {
	struct ed *ed = &ui.editor.ed;
	WINDOW *win = ui.editor.win;
	move_to_event_cell (channel, event, 0, 0);

	static char buf[0xff];
	char *p = buf;
	// Assuming there's a p4, and it's freq.
	if (event->cps)
		p += print_funcs[ed->notation] (p, event->cps);
	else
		p += sprintf (p, "N-A");
	*p++ = ' ';

	// Could use a macro for the width. Can't use an int without
	// fucking around with format strings. Actually, an int would be
	// better, but I'd need to fuck around with format strings.
	//
	// FIXME: it'd be nice to print leading zeroes as middle dots.

	p += snprintf (p, 32, "%02x", event->instr);
	*p++ = ' ';
	p += snprintf (p, 32, "%.2x", flt_to_hex (event->amp));
	*p++ = ' ';
	if (event->ps.d) {
		for (int i = 0; i < event->ps.n; i++) {
			// FIXME: it'd be nice to print leading zeroes as middle dots.
			p += snprintf (p, 32, "%02x%.2x ", event->ps.d[i].id,
					flt_to_hex (event->ps.d[i].val));
			// It doesn't hurt to add an extra space at the end.
			*p++ = ' ';
		}
	}
	*p++ = 0;
	wprintw (win, "%s", buf);
}

static void draw_cells (void) {
	struct sco *sco = &scos.d[scos.i];
	$each (channel, sco->channels.d, sco->channels.n) {
		$fori (i, channel->n)
			draw_cell (channel - sco->channels.d, &channel->d[i]);
	}
}

// Pass the parent window, not the statusline itself
static void draw_statusline (struct window *window) {

	auto statusline_win = window->statusline->win;
	wclear (statusline_win);
	wmove (statusline_win, 0, 0);
	if (window->type == WT_CL) {
		auto history = ui.cmdline.cl.current_history;
		auto current = &history->d[history->y];
		waddstr (statusline_win, history->name);
		wprintw (statusline_win, "\
history->y: %d, history->n: %zu, current->n: %zu, history->x: %d, buf: ",
				history->y, history->n, current->n, history->x);
		if (current->d)
			wprintw (statusline_win, "\"%s\"", current->d);
		else
			wprintw (statusline_win, "(null)", current->d);
	}
	else {
		auto sco = &scos.d[scos.i];
		auto ed = &ui.editor.ed;
		if (sco->path)
			wprintw (statusline_win, "%s", sco->path);
		else
			wprintw (statusline_win, "[No Name]", sco->path);
		mvwprintw (statusline_win, 0, 15, "%d", ed->octave);
		switch (ui.mode) {
			case M_NORMAL:
				mvwprintw (statusline_win, 0, 30, "mode: normal", ed->octave);
				break;
			case M_INSERT:
				mvwprintw (statusline_win, 0, 30, "mode: insert", ed->octave);
				break;
			case M_REPLACE:
				mvwprintw (statusline_win, 0, 30, "mode: replace", ed->octave);
				break;
			case M_REPLACE_ONE:
				mvwprintw (statusline_win, 0, 30, "mode: replace one", ed->octave);
				break;
			case M_PROMPT:
				mvwprintw (statusline_win, 0, 30, "mode: prompt", ed->octave);
				break;
			case M_CMDLINE_WINDOW_NORMAL:
				mvwprintw (statusline_win, 0, 30, "mode: cmdline window norm", ed->octave);
				break;
			case M_CMDLINE_WINDOW_INSERT:
				mvwprintw (statusline_win, 0, 30, "mode: cmdline window insert", ed->octave);
				break;
			default:
				break;
		}
	}
}

static void draw_top_border (void) {
	struct sco *sco = &scos.d[scos.i];
	WINDOW *win = ui.top_border.win;
	wmove (win, 0, 0);
	wclear (win);
	wprintw (ui.top_border.win, "    ");
	struct channels *channels = &sco->channels;
	$fori (i, channels->n) {
		waddch (ui.top_border.win, i + '0');
		int width = get_cell_width (&channels->d[i]);
		$fori (j, width - 1) // -1, cause we just printed the number
			waddch (ui.top_border.win, ' ');
	}
	refresh_top_bar ();
}

static void draw_editor (void) {
	WINDOW *win = ui.editor.win;
	wclear (win);
	draw_cells ();
	draw_left_border ();
	struct cl *cl = &ui.cmdline.cl;

	// TODO: shouldn't this info all be in the window, so I can just
	// do draw_window? It doesn't matter. This isn't a generic widget
	// library. Note, this ui.editor.* - 1 is necessary. It seems
	// wrong, but it's true.
	move_to_p_field_and_offset ();
	if (ui.mode == M_PFIELD_INSERT || ui.mode == M_INSTR_INSERT) {
		auto current = &cl->current_history->d[cl->current_history->n - 1];
		wprintw (win, "%.*s", current->n, current->d);
	}
	draw_statusline (&ui.editor);
	refresh_editor ();
}

static void draw_cmdline_window (void) {
	auto win = ui.cmdline_window.win;
	auto history = ui.cmdline.cl.current_history;
	wclear (win);
	wmove (win, 0, 0);
	int i = 0;
	for (; i < history->n; i++)
		wprintw (win, ":%.*s\n", history->d[i].n, history->d[i].d);

	// FIXME: shouldn't have to write 1000 ~s.
	while (i < 1000) {
		wprintw (win, "~\n");
		i++;
	}

	draw_statusline (&ui.cmdline_window);
	wmove (win, history->y, history->x + 1);
	refresh_cmdline_window ();
}

static void draw_prompt (void) {
	if (!ui.cmdline.cl.history.n)
		return;
	auto win = ui.cmdline.win; auto cl = &ui.cmdline.cl;
	auto history = cl->current_history; auto current = &history->d[history->n - 1];
	wclear (win);
	switch (ui.mode) {
		case M_PROMPT:
			mvwprintw (win, 0, 0, ":%.*s", current->n, current->d);
			break;
		case M_INSTR_INSERT:
			mvwprintw (win, 0, 0, "i%.*s", current->n, current->d);
			break;
		case M_AMP_INSERT:
			mvwprintw (win, 0, 0, "a%.*s", current->n, current->d);
			break;
		case M_PFIELD_INSERT:
			mvwprintw (win, 0, 0, "p%.*s", current->n, current->d);
			break;
		case M_CMDLINE_WINDOW_INSERT:
			mvwprintw (win, 0, 0, "-- INSERT --");
			break;
		default: break;
	}
	wmove (win, 0, history->x + 1);
	refresh_cmdline ();
}

void *ui_draw (void *arg) {
	struct timespec sleep_time = {
		.tv_sec = 0, .tv_nsec = 1000000000 / 60,
	};
	while (1) {
		if (!ui.changed) {
			nanosleep (&sleep_time, 0);
			continue;
		}

		// Note: when you pnoutrefresh, the cursor seems to move to
		// that window. So, the window you need want to show needs to
		// be the last one pnoutrefreshed. Hence, I only update the
		// one that you're on. Which saves cycles, anyway.

		switch (ui.mode) {
			case M_PROMPT:
			case M_INSTR_INSERT:
			case M_AMP_INSERT:
			case M_PFIELD_INSERT:
				draw_top_border ();
				draw_editor ();
				draw_prompt ();
				break;
			case M_CMDLINE_WINDOW_NORMAL:
			case M_CMDLINE_WINDOW_INSERT:
			case M_CMDLINE_WINDOW_REPLACE_ONE:
			case M_CMDLINE_WINDOW_REPLACE:
				draw_prompt ();
				draw_top_border ();
				draw_editor ();
				draw_cmdline_window ();
				break;
			case M_NORMAL:
			case M_INSERT:
			case M_REPLACE:
			case M_REPLACE_ONE:
				draw_top_border ();
				draw_editor ();
				break;
			default: break;
		}

		doupdate ();
		ui.changed = 0;
	}
}
// Doesn't bother with whatever's inside it. To be honest, if
// anything, window should be _inside_ ed, cmdline, etc.
struct window make_window (struct v dims, struct v pad_dims,
		enum window_type type) {
	struct window r = {
		.dims = dims,
		.win = newpad (pad_dims.y + 200, pad_dims.x),
		.type = type,
	};

	// ncurses documentation says don't use LINES and COLS, so I'll
	// use getmaxyx.
	int history, cols;
	getmaxyx (stdscr, history, cols);
	(void) history;
	if (type != WT_BOX) {
		r.statusline = malloc (sizeof *r.statusline);
		*r.statusline =  make_window (
				(struct v) {
					.w = cols, .h = 1
				},
				(struct v) {
					.y = 1, .x = cols * 2
				},
				WT_BOX);
	}
	if (!r.win)
		errx (1, "couldn't make ui item");
	keypad (r.win, 1);
	return r;
}

static struct history init_history (char *name) {
	struct history r = { .name = name };
	arr_push (&r, (struct line) {0});
	return r;
}

static struct cl init_cmdline (void) {
	struct cl r = {};
	r.history = init_history ("[Command Line]");
	r.pfield_history = init_history ("[P-Fields]");
	r.window_height = 7;
	return r;
}

void init_ui (void) {
	int rows, cols;
	getmaxyx (stdscr, rows, cols);
	ui = (struct ui) {
		.dims = {.w = cols, .h = rows},
		.editor = make_window ((struct v) {.w = cols - 1, .h = rows - 2},
				(struct v) {.y = 1000, .x = cols - 2}, WT_ED),
		.top_border = make_window ((struct v) {.w = cols - 1, .h = 1},
				(struct v) {.y = 1, .x = cols * 2}, WT_BOX),
		.cmdline = make_window ((struct v) {.w = cols - 1, .h = 1},
				(struct v) {.y = 10, .x = cols * 2}, WT_BOX),
		// FIXME: get rid of hardcoded pad size.
		.cmdline_window = make_window ((struct v) {.w = cols - 1, .h = 7},
				(struct v) {.y = 1000, .x = cols * 2}, WT_CL),
		.csound_msgs = make_window ((struct v) {.w = cols - 1, .h = 7},
				(struct v) {.y = 1000, .x = cols * 2}, WT_BOX),
	};
	ui.editor.ed = (struct ed) {
		.margin =  4,
		.cell_width = 24,
		.octave = 4,
	};

	ui.cmdline.cl = init_cmdline ();
	ui.cmdline.cl.current_history = &ui.cmdline.cl.history;
	pthread_t drawing_thread_id;
	ui.changed = 1;
	int ret = pthread_create (&drawing_thread_id, 0, ui_draw, 0);
	if (ret) // pthreads never set errno, apparently, so errx
		errx (1, "couldn't make drawing thread");
	ui_initialised = 1;
};

static void refresh_window (struct window *window, int screen_start_y,
		// This number includes the statusline, if there is one.
		// The statuslines are attached to the windows. I never
		// realised that was the case in Vim, even after about 7
		// years. That's why the cmdline window has a statusline.
		int screen_end_y) {
	int ret = 0;
	if (window->statusline) {
		ret |= pnoutrefresh (window->statusline->win, 0, 0,
				screen_end_y, 0, screen_end_y, window->dims.w - 1);
		ret = pnoutrefresh (window->win, window->pad_start.y, 0, screen_start_y,
				0, screen_end_y - 1, window->dims.w - 1);
	} else
		ret |= pnoutrefresh (window->win, window->pad_start.y, 0, screen_start_y,
				0, screen_end_y, window->dims.w - 1);
	if (ret)
		print_msg ("pnoutrefresh failed");
}

void refresh_cmdline (void) {
	// FIXME: Isn't this ui.dims.h, y wrong? It seems to be fine
	refresh_window (&ui.cmdline, ui.dims.h - 1, ui.dims.h - 1);
}

void refresh_editor (void) {
	int height = ui.dims.h - 2;
	if (ui.mode == M_CMDLINE_WINDOW_NORMAL)
		height -= ui.cmdline.cl.window_height;
	refresh_window (&ui.editor, 1, height);
}

void refresh_top_bar (void) {
	refresh_window (&ui.top_border, 0, 0);
}

void refresh_cmdline_window (void) {
	struct cl *cl = &ui.cmdline.cl;
	refresh_window (&ui.cmdline_window,

			// FIXME: this cl->window_height + 1 is to keep the editor
			// from clobbering its own statusline. I don't get it. My
			// numbers are wrong. It means that my window height is 6,
			// not 7, like Vim. I'm leaving it. Fucking around with
			// the window sizes is really fucking annoying.

			ui.dims.h - 2 - cl->window_height + 1, ui.dims.y - 2);
}

void refresh_all (void) {
	refresh_top_bar ();
	refresh_editor ();
	if (ui.mode == M_CMDLINE_WINDOW_NORMAL)
		refresh_cmdline_window ();
	refresh_cmdline ();
}

