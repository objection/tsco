#pragma once

#include "types.h"
#include "cmd.h"

extern bool ui_initialised;
int msg_height;

enum mode {
	M_NORMAL,
	M_INSERT,
	M_REPLACE,
	M_REPLACE_ONE,

	// In this, there's no cmdline insert mode, like in Vim. There's
	// just cmdline normal and cmdline insert. Keep it simple.
	M_PROMPT,
	M_CMDLINE_WINDOW_NORMAL,
	M_CMDLINE_WINDOW_INSERT,
	M_CMDLINE_WINDOW_REPLACE,
	M_CMDLINE_WINDOW_REPLACE_ONE,
	M_CPS_INSERT,
	M_INSTR_INSERT,
	M_AMP_INSERT,
	M_PFIELD_INSERT,
	M_HIT_ENTER,
};

enum ed_flags {
	EF_COMPILED = 1 << 0,
	EF_PLAYING = 1 << 1,
};

struct ed {
	enum ed_flags flags;
	int margin;
	int cell_width;
	int octave;

	// This notation should be an enum, but I'm not going to bother. I
	// want the .c to be, to some extent at least, independent.
	// They're not -- they all include types.h, and some of them
	// include other .h files, but let's keep it to a minimum.

	int notation;

};

enum window_type {
	WT_BOX, // ie, doesn't matter
	WT_ED,
	WT_CL,
};
struct window {
	struct v dims; // total
	struct v pad_start;
	WINDOW *win;
	union {
		struct ed ed;
		struct cl cl;
		void *whatever;
	};
	struct window *statusline;
	enum window_type type;
};

struct ui {
	enum mode mode;
	// Even though LINES and COLS are the truth of the term's dimensions,
	// I'll use this and update it every "frame".
	struct v dims;
	union {
		struct {
			struct window editor;
			struct window cmdline;
			struct window cmdline_window;
			struct window top_border;
			struct window statusline;
			struct window csound_msgs;
		};
		// I haven't used this.
		struct window windows[6];
	};
	bool changed;
	// This is just set in init_ncurses and is simply used to decide
	// whether to use printf or an ncurses function.
	bool initialised;
} extern ui;

void *ui_draw (void *arg);

void init_ui (void);
void refresh_cmdline (void);
void refresh_cmdline_window (void);
void refresh_statusline (void);
void refresh_editor (void);
void refresh_top_bar (void);
void refresh_all (void);
void clear_all (void);
void print_msg (char *fmt, ...);
void print_msg_ap (char *fmt, va_list ap);
