#pragma once

#include "types.h"

int str_compar (const void *a, const void *b);
int strn_compar_r (const void *a, const void *b, void *user);
bool is_all_space (size_t n, char *str);
char *get_skipnblanked (size_t n, char *p);
void skipnblank (char **_p, int n);
int flt_to_hex ($flt flt);
$flt hex_to_flt (int hex);
int next_multiple_of (int x, int of);
int strip (char *s, int n_s);
int n_chars_xs_in_str (char *s, char _the_char);

