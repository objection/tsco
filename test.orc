sr	=	48000
ksmps	=	1
nchnls	=	2
0dbfs	=	1

instr lead
	kamp = p5 * 0.05

	ipitch = p4 * 2
	kpitch_env = linseg(p6 * 1000, 0.4, 0.8)
	kpitch = ipitch + kpitch_env
	kpwm = linseg(0.25, 0.2, .50, 0.2, 0.25)
	asig = vco2(0.4 * 2, ipitch + kpitch, 16 + 2, kpwm, -1)
	kfilter_loopass_env = linseg(28000, 0.3, 5000, 0.5, 1000)
	afiltered = moogladder(asig, kfilter_loopass_env * .02, 0.2)
	ares = (asig + (afiltered * 0.5)) * kamp
	outs ares, ares
endin
